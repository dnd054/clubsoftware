<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 15:04
 */

namespace ClubSoftware\App;

use ClubSoftware\Helper\SessionHandler;
use \ClubSoftware\Model\User as UserModel;

class User
{
    private static $typeToRouteMap = [
        UserModel::TYPE_ADMIN => 'admin/welcome',
        UserModel::TYPE_CLIENT => 'client/menu',
        UserModel::TYPE_SOURCE => 'source/',
        UserModel::TYPE_PERSONEL => 'personel/orders',
    ];

    public static function isLogged()
    {
        return !empty($_SESSION['user']);
    }

    public static function logIn($data)
    {
        $_SESSION['user'] = $data;
    }

    public static function logOut()
    {
        SessionHandler::destroySession();
    }

    public static function getDefaultRoute()
    {
        if (!self::isLogged()) {
            return;
        }

        $user = $_SESSION['user'];
        return isset(self::$typeToRouteMap[$user->getRole()]) ? self::$typeToRouteMap[$user->getRole()] : null;
    }
}
