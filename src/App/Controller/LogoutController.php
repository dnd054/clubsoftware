<?php
namespace ClubSoftware\App\Controller;

use ClubSoftware\App\User;
use ClubSoftware\Mvc\LayoutController;

/**
 * Class LogoutController
 * @package App\Controller
 * @Route logout
 */
class LogoutController extends LayoutController
{
    public function doAction()
    {
        User::logOut();

        $this->redirect('/login');
    }
}