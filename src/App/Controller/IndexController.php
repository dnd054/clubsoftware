<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller;

use ClubSoftware\App\User;
use ClubSoftware\Mvc\LayoutController;

/**
 * Class WelcomeController
 * @package App\Controller
 * @Route /
 */
class IndexController extends LayoutController
{
    public function preAction()
    {
        if (User::isLogged()) {
            $route = '/' . User::getDefaultRoute();
            $this->redirect($route);
        }

        return parent::preAction();
    }

    public function doAction()
    {
        return $this->render('login/login');
    }
}
