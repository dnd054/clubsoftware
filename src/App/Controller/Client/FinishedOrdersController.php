<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\BasketOrder;
use ClubSoftware\Helper\OrderTable;
use ClubSoftware\Mvc\Template;

/**
 * Class FinishedOrdersController
 * @package App\ClientController
 * @Route client/orders/finished
 */
class FinishedOrdersController extends ClientController
{
    protected $finishedOrders = [];
    protected $finishedOrdersMaps = [];
    protected $layout = 'layouts/client';
    protected $title = 'Finished Orders';
    protected $finishedOrdersState = 'active';

    public function preAction()
    {
        if (!empty($_SESSION['finishedOrders'])) {
            $this->finishedOrders = $_SESSION['finishedOrders'];
        }
        if (!empty($_SESSION['finishedOrdersMaps'])) {
            $this->finishedOrdersMaps = $_SESSION['finishedOrdersMaps'];
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $finishedOrdersTables = [];

        if (!empty($this->finishedOrders)) {
            foreach ($this->finishedOrdersMaps as $key => $map) {
                $finishedOrderItems = BasketOrder::formatOrderItems($map);
                $finishedOrdersTables[] = new Template(OrderTable::getPath(), [
                    'order' => $this->finishedOrders[$key],
                    'orderItems' => $finishedOrderItems
                ]);
            }
        }

        return $this->render('client/orders/finished', [
            'orders' => $this->finishedOrders,
            'ordersTables' => $finishedOrdersTables
        ]);
    }
}