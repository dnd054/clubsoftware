<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\BasketOrder;
use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\MenuItemStorage;

/**
 * Class MenuController
 * @package App\ClientController
 * @Route menu
 */
class MenuController extends ClientController
{
    protected $categoryId;
    protected $layout = 'layouts/client';
    protected $title = 'Menu';
    protected $menuState = 'active';

    public function preAction()
    {
        if (!empty($_GET['categoryId'])) {
            $this->categoryId = $_GET['categoryId'];
        }

        return parent::preAction();
    }

    public function doAction()
    {
        if (!empty($_POST['quantity']) && !empty($_GET['productId'])) {
            $menuItemId = intval($_GET['productId']);
            $quantity = intval($_POST['quantity']);
            BasketOrder::add($menuItemId, $quantity);
            $this->redirect(Router::getUrl('/client/menu').'categoryId='.$this->categoryId);
        }

        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->where('category_id', $this->categoryId)
            ->offset($pager->getOffset())
            ->limit($limit);

        $menuItems = MenuItemStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('client/menu/index', [
            'menuItems' => $menuItems,
            'pager' => $pager
        ]);
    }
}