<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 18.3.2019 г.
 * Time: 9:16
 */

namespace ClubSoftware\App\Controller\Client;

abstract class OrderTableController extends ClientController
{
    public function getOrderTable()
    {
        $orderTable = '<table class="table table-bordered table-hover bg-light">';
            $orderTable .= '<thead class="thead-dark">';
            $orderTable .= '<tr>';
                $orderTable .= '<th>Name</th>';
                $orderTable .= '<th>Description</th>';
                $orderTable .= '<th id="fixed-quantity">Quantity</th>';
                $orderTable .= '<th>Price</th>';
                $orderTable .= '</tr>';
            $orderTable .= '</thead>';
            $orderTable .= '<tbody class="table table-bordered table-hover">';
                    $total = 0.0;

        foreach ($orderItems as $item) {
            $price = $item['price'];
            $total += ($price * $item['quantity']);

            $orderTable .= '<tr>';
                $orderTable .= '<td>'.$item['name'].'</td>';
                $orderTable .= '<td>'.$item['description'].'</td>';
                $orderTable .= '<td>'.$item['quantity'].'</td>';
                $orderTable .= '<td>$price ?></td>';
                $orderTable .= '</tr>';
        }
                    $orderTable .= '</tbody>';
                $orderTable .= '</table>';

            return $orderTable;
    }

    public function render($view, $params = [])
    {
        $params['orderTable'] = $this->getOrderTable();

        return parent::render($view, $params);
    }
}
