<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 23:48
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\BasketOrder;
use ClubSoftware\Model\Order;
use ClubSoftware\Model\OrderItem;
use ClubSoftware\Storage\ClientsTablesStorage;
use ClubSoftware\Storage\MenuItemStorage;
use ClubSoftware\Storage\OrderItemStorage;
use ClubSoftware\Storage\OrderStorage;
use ClubSoftware\Storage\TableStorage;

/**
 * Class BasketOrderController
 * @package App\ClientController
 * @Route client/basket/order
 */
class BasketOrderController extends ClientController
{
    protected $table;
    protected $layout = 'layouts/client';
    protected $title = 'Basket';
    protected $basketState = 'active';

    public function preAction()
    {
        $userId = $_SESSION['user']->getId();
        $tables = ClientsTablesStorage::getUsersTables($userId);
        $this->table = TableStorage::get($tables[0]->getIdTable());

        return parent::preAction();
    }

    public function doAction()
    {
        if (!empty($_SESSION['orderItemsMap'])) {
            $orderItemsMap = $_SESSION['orderItemsMap'];
            $order = new Order();
            $order->setTable($this->table);
            $order->setStatus(Order::STATUS_NEW);
            OrderStorage::add($order);
            $orderId = $order->getId();
            $orderItems = [];

            foreach ($orderItemsMap as $key => $value) {
                $menuItem = MenuItemStorage::get($key);
                for ($i = 0; $i < $value; $i++) {
                    $orderItem = new OrderItem($menuItem);
                    $orderItem->setOrderId($orderId);
                    $orderItem->setMenuItemId($menuItem->getId());
                    $orderItems[] = $orderItem;
                    OrderItemStorage::add($orderItem);
                }
            }

            $order->setOrderItems($orderItems);
            $_SESSION['currentOrder'] = $order;
            $_SESSION['currentMap'] = $orderItemsMap;

            BasketOrder::clearBasket();
            $this->redirect('/client/orders/current');
        }

        return $this->render('client/basket/index');
    }
}
