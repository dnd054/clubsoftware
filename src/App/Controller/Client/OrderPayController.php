<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 30.3.2019 г.
 * Time: 0:54
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Model\Order;
use ClubSoftware\Storage\OrderStorage;

class OrderPayController extends ClientController
{
    protected $layout = 'layouts/client';
    protected $title = 'Orders';
    protected $ordersState = 'active';

    public function doAction()
    {
        $currentOrder = $_SESSION['currentOrder'];
        $currentMap = $_SESSION['currentMap'];
        $newValues = ['status' => Order::STATUS_FINISHED];
        OrderStorage::update($currentOrder, $newValues);
        $currentOrder->setStatus(Order::STATUS_FINISHED);
        $_SESSION['finishedOrders'][] = $currentOrder;
        $_SESSION['finishedOrdersMaps'][] = $currentMap;
        unset($_SESSION['currentOrder']);
        unset($_SESSION['currentMap']);

        $this->redirect('/client/orders/finished');
    }
}
