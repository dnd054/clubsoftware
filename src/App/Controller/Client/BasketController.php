<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\BasketOrder;

/**
 * Class BasketController
 * @package App\ClientController
 * @Route basket
 */
class BasketController extends ClientController
{
    protected $layout = 'layouts/client';
    protected $title = 'Basket';
    protected $basketState = 'active';

    public function doAction()
    {
        $basketItems = [];

        if (!empty($_SESSION['orderItemsMap'])) {
            $orderItemsMap = $_SESSION['orderItemsMap'];
            $basketItems = BasketOrder::formatOrderItems($orderItemsMap);
        }

        return $this->render('client/basket/index', ['basketItems' => $basketItems]);
    }
}