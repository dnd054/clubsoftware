<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\BasketOrder;
use ClubSoftware\Helper\OrderTable;
use ClubSoftware\Mvc\Template;

/**
 * Class CurrentOrderController
 * @package App\ClientController
 * @Route client/orders/current
 */
class CurrentOrderController extends ClientController
{
    protected $currentOrder = [];
    protected $currentMap = [];
    protected $layout = 'layouts/client';
    protected $title = 'Current Order';
    protected $currentOrderState = 'active';

    public function preAction()
    {
        if (!empty($_SESSION['currentOrder'])) {
            $this->currentOrder = $_SESSION['currentOrder'];
        }
        if (!empty($_SESSION['currentMap'])) {
            $this->currentMap = $_SESSION['currentMap'];
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $orderItems = BasketOrder::formatOrderItems($this->currentMap);
        $orderTable = new Template(OrderTable::getPath(), [
            'order' => $this->currentOrder,
            'orderItems' => $orderItems
            ]);

        return $this->render('client/orders/current', [
            'currentOrder' => $this->currentOrder,
            'orderTable' => $orderTable
        ]);
    }
}
