<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\BasketOrder;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;

/**
 * Class BasketController
 * @package App\ClientController
 * @Route client/basket/delete
 */
class BasketDeleteController extends LayoutController
{
    public function doAction()
    {
        $productId = $_GET['productId'];
        BasketOrder::remove($productId);

        $this->redirect(Router::getUrl('/client/basket'));
    }
}