<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 18.3.2019 г.
 * Time: 9:16
 */

namespace ClubSoftware\App\Controller\Client;

use ClubSoftware\Helper\CategoriesHelper;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\ClientsTablesStorage;

abstract class ClientController extends LayoutController
{
    protected $menuState = '';
    protected $basketState = '';
    protected $currentOrderState = '';
    protected $finishedOrdersState = '';

    public function hasChildren($categories, $id)
    {
        foreach ($categories as $category) {
            if ($category->getParentId() == $id) {
                return true;
            }
        }

        return false;
    }

    public function getCategoryTree($categories, $parent = '')
    {
        $categoryTree = '';
        foreach ($categories as $category) {
            if ($category->getParentId() == $parent) {
                if ($this->hasChildren($categories, $category->getId())) {
                    $ulClass = 'submenu';
                    $categoryTree .= '<li class="border-bottom border-primary">
                    <a href="#">'.$category->getName().'</a>';
                    $categoryTree .= '<ul class="'.$ulClass.'">'.$this->getCategoryTree($categories, $category->getId()).'</ul>';
                    $categoryTree .= '</li>';
                } else {
                    $categoryTree .= '<li><a href="'.Router::getUrl('/client/menu', ['categoryId' => $category->getId()]).'">
                    '.$category->getName().'</a>';
                    $categoryTree .= '</li>';
                }
            }
        }

        return $categoryTree;
    }

    public function getTabs()
    {
        $tabs = '<a class="nav-item nav-link '.$this->menuState.'" 
href="'.Router::getUrl("/client/menu").'" role="tab">Menu</a>';
        $tabs .= '<a class="nav-item nav-link '.$this->basketState.'" 
href="'.Router::getUrl("/client/basket").'" role="tab">Basket</a>';
        $tabs .= '<a class="nav-item nav-link '.$this->currentOrderState.'"
href="'.Router::getUrl("/client/orders/current").'" role="tab">Current Order</a>';
        $tabs .= '<a class="nav-item nav-link '.$this->finishedOrdersState.'"
href="'.Router::getUrl("/client/orders/finished").'" role="tab">Finished Orders</a>';

        return $tabs;
    }

    public function render($view, $params = [])
    {
        $categories = CategoriesHelper::all();
        $params['categoryTree'] = $this->getCategoryTree($categories);
        $params['tabs'] = $this->getTabs();

        return parent::render($view, $params);
    }
}
