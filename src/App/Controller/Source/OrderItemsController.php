<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Source;

use ClubSoftware\Db\JoinDescriptor;
use ClubSoftware\Db\Query;
use ClubSoftware\Helper\OrderItemDropdownStatusButton;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\OrderItemStorage;
use ClubSoftware\Storage\SourceStorage;

/**
 * Class OrderItemsController
 * @package App\SourceController
 * @Route source
 */
class OrderItemsController extends LayoutController
{
    private $source;
    protected $layout = 'layouts/source';
    protected $title = 'Order Items';

    public function preAction()
    {
        $userId = $_SESSION['user']->getId();
        $query = (new Query())
            ->where('user_id', $userId)
        ;
        $this->source = SourceStorage::all($query);

        return parent::preAction();
    }

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);
        $sourceName = $this->source[0]->getName();

        $query = (new Query())
            ->select('order_items.id')
            ->select('order_items.menu_item_id')
            ->select('order_items.status')
            ->select('menu_items.source_id')
            ->from('order_items')
            ->join(JoinDescriptor::LEFT_JOIN, 'menu_items', 'order_items.menu_item_id = menu_items.id')
            ->where('source_id', $this->source[0]->getId())
            ->orderBy('order_items.id DESC')
            ->offset($pager->getOffset())
            ->limit($limit);

        $rawOrderItems = OrderItemStorage::all($query);
        $rows = $pager->queryCount($query);
        $orderItems = $this->makeTable($rawOrderItems);

        return $this->render('source/index', [
            'sourceName' => $sourceName,
            'orderItems' => $orderItems,
            'pager' => $pager]);
    }

    private function makeTable($rawOrderItems)
    {
        $statusButton = new OrderItemDropdownStatusButton();
        $orderItems = [];

        foreach ($rawOrderItems as $orderItem) {
            $row = '<tr>';
            $row .= '<td id="fixed-id">'.$orderItem->getId().'</td>';
            $row .= '<td>'.$orderItem->getMenuItem()->getName().'</td>';
            $row .= '<td>'.$orderItem->getMenuItem()->getDescription().'</td>';
            $row .= '<td id="fixed-actions">'.$statusButton->getButton($orderItem->getStatus(), $orderItem->getId()).'</td>';
            $row .= '</tr>';

            $orderItems[] = $row;
        }

        return $orderItems;
    }
}