<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 26.4.2019 г.
 * Time: 14:14
 */

namespace ClubSoftware\App\Controller\Source;

use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\OrderItemStorage;

class OrderItemStatusController extends LayoutController
{
    private $model;
    private $statusId;

    public function preAction()
    {
        $orderItemId = $_GET['orderItemId'];
        $this->statusId = $_GET['statusId'];
        $this->model = OrderItemStorage::get($orderItemId);

        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }
    public function doAction()
    {
        $newValue = ['status' => $this->statusId];

        OrderItemStorage::update($this->model, $newValue);
        $url = Router::getUrl('/source');
        $this->redirect($url);
    }
}
