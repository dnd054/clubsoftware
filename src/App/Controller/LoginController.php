<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:49
 */

namespace ClubSoftware\App\Controller;

use ClubSoftware\App\User;
use ClubSoftware\Db\Query;
use ClubSoftware\Helper\SessionHandler;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\UserStorage;

/**
 * Class LoginController
 * @package App\Controller
 * @Route login
 */
class LoginController extends LayoutController
{
    protected $title = 'Login';

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $errors = $this->handlePost();
        }

        return $this->render('login/login', ['errors' => $errors]);
    }

    private function handlePost()
    {
        $wrongUsername = 'Wrong Username or Password!';
        $userLogged = 'This user has already logged in!';
        $errors = [];
        $userName = empty($_POST['username']) ? '' : $_POST['username'];
        $password = empty($_POST['password']) ? '' : $_POST['password'];
        $user = current(UserStorage::all((new Query())->where('username', $userName)));
        if (!$user) {
            $errors['login'][] = $wrongUsername;

            return $errors;
        }

        if (!SessionHandler::checkSessionExpired($user->getLastActive())) {
            $errors['login'][] = $userLogged;

            return $errors;
        }

        if (!password_verify($password, $user->getPassword())) {
            $errors['login'][] = $wrongUsername;

            return $errors;
        }
        User::logIn($user);
        SessionHandler::updateSession();
        $this->redirect(User::getDefaultRoute());

        return $errors;
    }
}
