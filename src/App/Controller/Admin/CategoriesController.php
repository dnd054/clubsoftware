<?php

/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.02.19
 * Time: 13:46
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\CategoryStorage;

/**
 * Class CategoriesController
 * @package App\AdminController
 * @Route admin/categories/index
 */
class CategoriesController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Categories';

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->offset($pager->getOffset())
            ->limit($limit)
            ->orderBy('parent_id');

        $categories = CategoryStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('admin/categories/index', ['categories' => $categories, 'pager' => $pager]);
    }
}