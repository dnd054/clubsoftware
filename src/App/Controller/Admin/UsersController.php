<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\UserStorage;

/**
 * Class UsersController
 * @package App\AdminController
 * @Route users
 */
class UsersController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Users';

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->offset($pager->getOffset())
            ->limit($limit);
        $users = UserStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('admin/users/index', ['users' => $users, 'pager' => $pager]);
    }
}