<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\MenuItemStorage;

/**
 * Class MenuController
 * @package App\AdminController
 * @Route menu
 */
class MenuController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Menu';

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->offset($pager->getOffset())
            ->limit($limit);

        $menuItems = MenuItemStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('admin/menu/index', ['menuItems' => $menuItems, 'pager' => $pager]);
    }
}