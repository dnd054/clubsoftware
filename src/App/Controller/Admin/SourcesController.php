<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 12.4.2019 г.
 * Time: 16:58
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\SourceStorage;

/**
 * Class SourcesController
 * @package App\AdminController
 * @Route admin/sources/index
 */

class SourcesController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Sources';

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->offset($pager->getOffset())
            ->limit($limit)
        ;
        $sources = SourceStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('admin/sources/index', ['sources' => $sources, 'pager' => $pager]);
    }
}
