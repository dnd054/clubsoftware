<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\DeleteAction;
use ClubSoftware\Storage\OrderStorage;

class OrderDeleteController extends DeleteAction
{
    protected $storage = OrderStorage::class;
    protected $redirectPath = '/admin/orders';
}
