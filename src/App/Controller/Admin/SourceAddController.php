<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 12.4.2019 г.
 * Time: 16:58
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\Source;
use ClubSoftware\Model\User;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\SourceStorage;
use ClubSoftware\Storage\UserStorage;

class SourceAddController extends LayoutController
{
    private $source;
    private $sourceUsers;
    protected $layout = 'layouts/admin';
    protected $title = 'Add Source';

    public function preAction()
    {
        $this->source = new Source();
        $query = (new Query())
            ->where('role', User::TYPE_SOURCE)
            ;
        $this->sourceUsers = UserStorage::all($query);

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $sourceName = ParamsHelper::getPostParam('name', '');
            $userId = ParamsHelper::getPostParam('userId', '');
            $errors = $this->validate($sourceName);

            if (empty($errors)) {
                $this->source->setName($sourceName);
                $this->source->setUserId($userId);

                SourceStorage::add($this->source);
                $url = Router::getUrl('/admin/sources');
                $this->redirect($url);
            }
        }

        return $this->render('admin/sources/add', [
            'source' => $this->source,
            'sourceUsers' => $this->sourceUsers,
            'errors' => $errors
        ]);
    }

    private function validate($sourceName)
    {
        $errors = [];

        if (mb_strlen($sourceName, 'UTF-8') < 3 || mb_strlen($sourceName, 'UTF-8') > 10) {
            $errors['name'][] = 'Source name must be between 3 and 10 letters!';
        }

        return $errors;
    }
}