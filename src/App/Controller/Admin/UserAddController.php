<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 6.3.2019 г.
 * Time: 9:06
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Helper\UsersTablesStorage;
use ClubSoftware\Model\User;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\ClientsTablesStorage;
use ClubSoftware\Storage\UserStorage;

class UserAddController extends LayoutController
{
    private $user;
    private $freeTables;
    protected $layout = 'layouts/admin';
    protected $title = 'Add Users';

    public function preAction()
    {
        $this->user = new User();
        $this->freeTables = ClientsTablesStorage::getFreeTables();

        return parent::preAction();
    }

    public function doAction()
    {
        $tableId = $this->freeTables[0]->getId();
        $errors = [];

        if (!empty($_POST)) {
            $username = ParamsHelper::getPostParam('username', '');
            $role = ParamsHelper::getPostParam('role', '');
            $newPassword = ParamsHelper::getPostParam('new_password', '');
            $repeatPassword = ParamsHelper::getPostParam('confirm_password', '');
           
            $errors = $this->validate($username, $role, $newPassword, $repeatPassword, $tableId);

            if (empty($errors)) {
                $this->user->setUsername($username);
                $this->user->setRole($role);
                if (!empty($newPassword) && $newPassword == $repeatPassword) {
                    $this->user->setPassword(password_hash($newPassword, PASSWORD_DEFAULT));
                }

                UserStorage::add($this->user);
                if ($role == User::TYPE_CLIENT) {
                    $userId = UserStorage::getLastId();
                    ClientsTablesStorage::assignTable($userId, $tableId);
                }
                $url = Router::getUrl('/admin/users');
                $this->redirect($url);
            }
        }

        return $this->render('admin/users/add', [
            'user' => $this->user,
            'errors' => $errors
        ]);
    }

    private function validate($username, $role, $newPassword, $repeatPassword, $tableId)
    {
        $errors = [];

        if (mb_strlen($username, 'UTF-8') < 3 || mb_strlen($username, 'UTF-8') > 20) {
            $errors['username'][] = 'Username must be between 3 and 20 letters!';
        }

        if (!in_array($role, [User::TYPE_CLIENT, User::TYPE_ADMIN, User::TYPE_SOURCE, User::TYPE_PERSONEL])) {
            $errors['role'][] = 'Invalid Role!';
        }

        if (empty($newPassword)) {
            $errors['new_password'][] = 'Password Cannot Be Empty';
            return $errors;
        }

        if (mb_strlen($newPassword, 'UTF-8') < 5 || mb_strlen($newPassword, 'UTF-8') > 10) {
            $errors['new_password'][] = 'Password must be between 5 and 10 characters!';
        }

        if ($newPassword != $repeatPassword) {
            $errors['repeat_password'][] = 'Passwords don\'t match!';
        }

        if ($role == User::TYPE_CLIENT && empty($tableId)) {
            $errors['freeTable'][] = 'No free table for new client!';
        }

        return $errors;
    }
}