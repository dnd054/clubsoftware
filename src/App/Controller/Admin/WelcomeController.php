<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Mvc\LayoutController;

/**
 * Class WelcomeController
 * @package App\AdminController
 * @Route welcome
 */
class WelcomeController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Admin';

    public function doAction()
    {
        return $this->render('admin/welcome/index');
    }
}