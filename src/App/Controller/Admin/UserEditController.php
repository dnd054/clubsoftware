<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Helper\UsersTablesStorage;
use ClubSoftware\Model\User;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\UserStorage;

class UserEditController extends LayoutController
{
    private $model;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Users';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = UserStorage::get($id);
        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $username = ParamsHelper::getPostParam('username', '');
            $role = ParamsHelper::getPostParam('role', '');
            $newPassword = ParamsHelper::getPostParam('new_password', '');
            $repeatPassword = ParamsHelper::getPostParam('confirm_password', '');

            $errors = $this->validate($username, $role, $newPassword, $repeatPassword);

            if (empty($errors)) {
                $newValues = ['username' => $username, 'role' => $role];
                if (!empty($newPassword) && $newPassword == $repeatPassword) {
                    $newValues['password'] = password_hash($newPassword, PASSWORD_DEFAULT);
                }
                UserStorage::update($this->model, $newValues);
                $url = Router::getUrl('/admin/users');
                $this->redirect($url);
            }
        }

        return $this->render('admin/users/edit', [
            'model' => $this->model,
            'errors' => $errors
        ]);
    }

    private function validate($username, $role, $newPassword, $repeatPassword)
    {
        $errors = [];

        if (mb_strlen($username, 'UTF-8') < 3 || mb_strlen($username, 'UTF-8') > 20) {
            $errors['username'][] = 'Username must be between 3 and 20 letters!';
        }

        if (!in_array($role, [User::TYPE_CLIENT, User::TYPE_ADMIN, User::TYPE_SOURCE, User::TYPE_PERSONEL])) {
            $errors['role'][] = 'Invalid Role!';
        }

        if (empty($newPassword)) {
            $errors['new_password'][] = 'Password Cannot Be Empty';
            return $errors;
        }

        if (mb_strlen($newPassword, 'UTF-8') < 5 || mb_strlen($newPassword, 'UTF-8') > 10) {
            $errors['new_password'][] = 'Password must be between 5 and 10 characters!';
        }

        if ($newPassword != $repeatPassword) {
            $errors['repeat_password'][] = 'Passwords don\'t match!';
        }

        return $errors;
    }
}