<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\OrderItemStorage;

/**
 * Class OrderItemsEditController
 * @package App\AdminController
 * @Route admin/orders/orderItems
 */
class OrderItemsEditController extends LayoutController
{
    private $model;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Tables';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = OrderItemStorage::get($id);
        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $status = ParamsHelper::getPostParam('status', '');

            $errors = $this->validate($status);

            if (empty($errors)) {
                $newValue = ['status' => $status];

                OrderItemStorage::update($this->model, $newValue);
                $url = Router::getUrl('/admin/orders/orderItems', ['id' => $this->model->getOrderId()]);
                $this->redirect($url);
            }
        }

        return $this->render('admin/orders/orderItems/edit', [
            'model' => $this->model,
            'errors' => $errors
        ]);
    }

    private function validate($status)
    {
        $errors = [];

        if (empty($status)) {
            $errors['status'][] = 'Please select status!';
        }

        return $errors;
    }
}