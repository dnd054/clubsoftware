<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\TableStorage;

/**
 * Class TablesController
 * @package App\AdminController
 * @Route /admin/tables
 */
class TablesController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Tables';

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->offset($pager->getOffset())
            ->limit($limit);
        $tables = TableStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('admin/tables/index', ['tables' => $tables, 'pager' => $pager]);
    }
}