<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\OrderItemStorage;
use ClubSoftware\Storage\OrderStorage;

/**
 * Class OrderItemsController
 * @package App\AdminController
 * @Route admin/orders/orderItems
 */
class OrderItemsController extends LayoutController
{
    protected $order;
    protected $orderId;
    protected $layout = 'layouts/admin';
    protected $title = 'Order Items';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->orderId = $id;

        return parent::preAction();
    }

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);
        $order = OrderStorage::get($this->orderId);

        $query = (new Query())
            ->where('order_id', $this->orderId)
            ->offset($pager->getOffset())
            ->limit($limit);

        $orderItems = OrderItemStorage::all($query);
        $order->setOrderItems($orderItems);
        $rows = $pager->queryCount($query);

        return $this->render('admin/orders/orderItems/index', ['order' => $order, 'pager' => $pager]);
    }
}