<?php

/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.02.19
 * Time: 13:46
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\DeleteAction;
use ClubSoftware\Storage\CategoryStorage;

/**
 * Class CategoryDeleteController
 * @package App\AdminController
 * @Route admin/categories/delete
 */
class CategoryDeleteController extends DeleteAction
{
    protected $storage = CategoryStorage::class;
    protected $redirectPath = '/admin/categories';
}