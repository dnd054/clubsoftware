<?php

/**
 * Created by PhpStorm.
 * User: DND
 * Date: 16.5.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Model\User;
use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\UserStorage;

/**
 * Class PersonelTablesController
 * @package App\AdminController
 * @Route /admin/personel
 */
class PersonelTablesController extends LayoutController
{
    protected $layout = 'layouts/admin';
    protected $title = 'Personel';

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);

        $query = (new Query())
            ->where('role', User::TYPE_PERSONEL)
            ->offset($pager->getOffset())
            ->limit($limit);
        $personel = UserStorage::all($query);
        $rows = $pager->queryCount($query);

        return $this->render('admin/personel/index', ['personel' => $personel, 'pager' => $pager]);
    }
}