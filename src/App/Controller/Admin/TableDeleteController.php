<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\DeleteAction;
use ClubSoftware\Storage\TableStorage;

class TableDeleteController extends DeleteAction
{
    protected $storage = TableStorage::class;
    protected $redirectPath = '/admin/tables';
}
