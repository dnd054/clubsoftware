<?php

/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.02.19
 * Time: 13:46
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\CategoriesHelper;
use ClubSoftware\Helper\CategoryList;
use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\Category;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\CategoryStorage;

/**
 * Class CategoryAddController
 * @package App\AdminController
 * @Route admin/categories/add
 */
class CategoryAddController extends LayoutController
{
    protected $category;
    protected $layout = 'layouts/admin';
    protected $title = 'Categories';

    public function preAction()
    {
        $this->category = new Category();

        return parent::preAction();
    }

    public function doAction()
    {
        $categoryList = new CategoryList();

        $errors = [];
    
        if (!empty($_POST)) {
            $name = ParamsHelper::getPostParam('name', '');

            $errors = $this->validate($name);
        
            if (empty($errors)) {
                $this->category->setName($name);
                if (empty($_POST['parentId'])) {
                    $this->category->setPath('');
                } else {
                    $this->category->setParentId($_POST['parentId']);
                    $parent = CategoryStorage::get($_POST['parentId']);
                    $newPath = [$parent->getPath(), $parent->getId()];
                    $path = implode(CategoriesHelper::PATH_SEPARATOR, $newPath);
                    $this->category->setPath($path);
                }

                CategoryStorage::add($this->category);
                $url = Router::getUrl('/admin/categories');
                $this->redirect($url);
            }
        }

        return $this->render('admin/categories/add', [
            'categoryList' => $categoryList,
            'errors' => $errors
        ]);
    }

    private function validate($name)
    {
        $errors = [];

        if (mb_strlen($name, 'UTF-8') < 3 || mb_strlen($name, 'UTF-8') > 15) {
            $errors['name'][] = 'Category name must be between 3 and 15 letters!';
        }

        return $errors;
    }
}
