<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\OrderItem;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\MenuItemStorage;
use ClubSoftware\Storage\OrderItemStorage;

/**
 * Class OrderItemsAddController
 * @package App\AdminController
 * @Route admin/orders/orderItems/add
 */
class OrderItemsAddController extends LayoutController
{
    private $orderItem;
    private $menuItems;
    private $orderId;
    protected $layout = 'layouts/admin';
    protected $title = 'Add Order Item';

    public function preAction()
    {
        $this->orderId = $_GET['orderId'];
        $this->orderItem = new OrderItem();
        $this->menuItems = MenuItemStorage::all();

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $menuItemId = ParamsHelper::getPostParam('menuItemId', '');

            $errors = $this->validate($menuItemId);

            if (empty($errors)) {
                $this->orderItem->setMenuItemId($menuItemId);
                $this->orderItem->setOrderId($this->orderId);

                OrderItemStorage::add($this->orderItem);
                $url = Router::getUrl('/admin/orders/orderItems', ['id' => $this->orderId]);
                $this->redirect($url);
            }
        }

        return $this->render('admin/orders/orderItems/add', [
            'orderItem' => $this->orderItem,
            'menuItems' => $this->menuItems,
            'errors' => $errors
        ]);
    }

    private function validate($menuItemId)
    {
        $errors = [];

        if (empty($menuItemId)) {
            $errors['menuItemId'][] = 'Please select menu item!';
        }

        return $errors;
    }
}