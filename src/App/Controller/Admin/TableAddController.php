<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 6.3.2019 г.
 * Time: 9:06
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\Table;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\TableStorage;

class TableAddController extends LayoutController
{
    private $table;
    protected $layout = 'layouts/admin';
    protected $title = 'Add Tables';

    public function preAction()
    {
        $this->table = new Table();

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $tableName = ParamsHelper::getPostParam('tableName', '');
            $errors = $this->validate($tableName);

            if (empty($errors)) {
                $this->table->setName($tableName);

                TableStorage::add($this->table);
                $url = Router::getUrl('/admin/tables');
                $this->redirect($url);
            }
        }

        return $this->render('admin/tables/add', ['table' => $this->table, 'errors' => $errors]);
    }

    private function validate($tableName)
    {
        $errors = [];

        if (mb_strlen($tableName, 'UTF-8') < 3 || mb_strlen($tableName, 'UTF-8') > 10) {
            $errors['tableName'][] = 'Table name must be between 3 and 10 letters!';
        }

        return $errors;
    }
}