<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 6.3.2019 г.
 * Time: 9:06
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\CategoryList;
use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\MenuItem;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\MenuItemStorage;
use ClubSoftware\Storage\SourceStorage;
use Intervention\Image\ImageManagerStatic as Image;

class MenuAddController extends LayoutController
{
    private $menuItem;
    private $menuItemId;
    private $sources;
    private $categoryList;
    protected $layout = 'layouts/admin';
    protected $title = 'Add Menu Item';

    public function preAction()
    {
        $this->sources = SourceStorage::all();
        $this->menuItem = new MenuItem();
        $this->categoryList = new CategoryList();
        $this->menuItemId = MenuItemStorage::getLastId()+1;

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $itemName = ParamsHelper::getPostParam('itemName', '');
            $description = ParamsHelper::getPostParam('description', '');
            $price = ParamsHelper::getPostParam('price', '');
            $source = ParamsHelper::getPostParam('sourceId', '');
            $categoryId = ParamsHelper::getPostParam('parentId', '');

            $errors = $this->validate($itemName, $description, $price, $source);

            if (empty($errors)) {
                $this->menuItem->setName($itemName);
                $this->menuItem->setDescription($description);
                $this->menuItem->setPrice($price);
                $this->menuItem->setSourceId($source);
                $this->menuItem->setCategoryId($categoryId);

                $webPath = realpath(__DIR__ . '/../../../../web');
                $imgPath = '/views/images/products';
                $tmpPath = $_FILES['image']['tmp_name'];
                $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if (!in_array($extension, ['jpg', 'jpeg', 'png'])) {
                    $errors['image'][] = 'Wrong Extension!';
                } else {
                    $fileName = $imgPath . '/' . $this->menuItemId . '.' . pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                    $img = Image::make($tmpPath);
                    $img->resize(254, 150);
                    $img->save($webPath . $fileName);
                    $this->menuItem->setImgPath($fileName);

                    MenuItemStorage::add($this->menuItem);
                    $url = Router::getUrl('/admin/menu');
                    $this->redirect($url);
                }
            }
        }

        return $this->render('admin/menu/add', [
            'menuItem' => $this->menuItem,
            'sources' => $this->sources,
            'categoryList' => $this->categoryList,
            'errors' => $errors
        ]);
    }

    private function validate($itemName, $description, $price, $source)
    {
        $errors = [];

        if (mb_strlen($itemName, 'UTF-8') < 3 || mb_strlen($itemName, 'UTF-8') > 20) {
            $errors['itemName'][] = 'Item name must be between 3 and 20 letters!';
        }

        if (mb_strlen($description, 'UTF-8') > 50) {
            $errors['description'][] = 'Description must be no more than 50 letters!';
        }

        if (empty($price)) {
            $errors['price'][] = 'Price Cannot Be Empty';
            return $errors;
        }

        if (empty($source)) {
            $errors['source'][] = 'Please select source!';
        }

        return $errors;
    }
}