<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\OrderStorage;
use ClubSoftware\Storage\TableStorage;

/**
 * Class OrderEditController
 * @package App\AdminController
 * @Route admin/orders/edit
 */
class OrderEditController extends LayoutController
{
    protected $model;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Orders';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = OrderStorage::get($id);
        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $tables = TableStorage::all();

        if (!empty($_POST)) {
            $tableId = ParamsHelper::getPostParam('tableId');
            $status = ParamsHelper::getPostParam('status');

            $errors = $this->validate($status);

            if (empty($errors)) {
                $newValues = ['table_id' => $tableId, 'status' => $status];

                OrderStorage::update($this->model, $newValues);
                $url = Router::getUrl('/admin/orders');
                $this->redirect($url);
            }
        }

        return $this->render('admin/orders/edit', [
            'model' => $this->model,
            'tables' => $tables,
            'errors' => $errors
        ]);
    }

    private function validate($status)
    {
        $errors = [];

        if (empty($status)) {
            $errors['status'][] = 'Status cannot be empty!';
        }

        return $errors;
    }
}