<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\OrderItemStorage;

/**
 * Class CurrentOrderController
 * @package App\AdminController
 * @Route admin/orders/orderItems
 */
class OrderItemsDeleteController extends LayoutController
{
    private $orderItemId;
    private $orderId;

    public function preAction()
    {
        $this->orderItemId = $_GET['id'];
        $this->orderId = OrderItemStorage::get($this->orderItemId)->getOrderId();

        return parent::preAction();
    }

    public function doAction()
    {
        OrderItemStorage::delete($this->orderItemId);
        $url = '/admin/orders/orderItems';
        $this->redirect(Router::getUrl($url).'id='.$this->orderId);
    }
}
