<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\DeleteAction;
use ClubSoftware\Storage\SourceStorage;

class SourceDeleteController extends DeleteAction
{
    protected $storage = SourceStorage::class;
    protected $redirectPath = '/admin/sources';
}
