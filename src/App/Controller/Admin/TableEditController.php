<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\TableStorage;

class TableEditController extends LayoutController
{
    private $model;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Tables';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = TableStorage::get($id);
        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $tableName = ParamsHelper::getPostParam('name', '');

            $errors = $this->validate($tableName);

            if (empty($errors)) {
                $newValue = ['name' => $tableName];

                TableStorage::update($this->model, $newValue);
                $url = Router::getUrl('/admin/tables');
                $this->redirect($url);
            }
        }

        return $this->render('admin/tables/edit', ['model' => $this->model, 'errors' => $errors]);
    }

    private function validate($tableName)
    {
        $errors = [];

        if (mb_strlen($tableName, 'UTF-8') < 3 || mb_strlen($tableName, 'UTF-8') > 10) {
            $errors['name'][] = 'Table name must be between 3 and 10 letters!';
        }

        return $errors;
    }
}