<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\DeleteAction;
use ClubSoftware\Storage\UserStorage;

class UserDeleteController extends DeleteAction
{
    protected $storage = UserStorage::class;
    protected $redirectPath = '/admin/users';
}
