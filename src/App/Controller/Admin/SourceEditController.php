<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 5.15.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\User;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\SourceStorage;
use ClubSoftware\Storage\UserStorage;

class SourceEditController extends LayoutController
{
    private $model;
    private $sourceUsers;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Sources';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = SourceStorage::get($id);
        if (empty($this->model)) {
            return false;
        }
        $query = (new Query())
            ->where('role', User::TYPE_SOURCE)
        ;
        $this->sourceUsers = UserStorage::all($query);

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $sourceName = ParamsHelper::getPostParam('name', '');
            $userId = ParamsHelper::getPostParam('userId', '');

            $errors = $this->validate($sourceName);

            if (empty($errors)) {
                $newValues = ['name' => $sourceName, 'user_id' => $userId];

                SourceStorage::update($this->model, $newValues);
                $url = Router::getUrl('/admin/sources');
                $this->redirect($url);
            }
        }

        return $this->render('admin/sources/edit', [
            'model' => $this->model,
            'sourceUsers' => $this->sourceUsers,
            'errors' => $errors]);
    }

    private function validate($sourceName)
    {
        $errors = [];

        if (mb_strlen($sourceName, 'UTF-8') < 3 || mb_strlen($sourceName, 'UTF-8') > 20) {
            $errors['name'][] = 'Source name must be between 3 and 20 letters!';
        }

        return $errors;
    }
}