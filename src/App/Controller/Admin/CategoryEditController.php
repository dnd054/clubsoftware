<?php

/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.02.19
 * Time: 13:46
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\CategoryStorage;

/**
 * Class CategoryEditController
 * @package App\AdminController
 * @Route admin/categories/edit
 */
class CategoryEditController extends LayoutController
{
    private $model;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Categories';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = CategoryStorage::get($id);
        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $categoryName = ParamsHelper::getPostParam('name', '');
            $errors = $this->validate($categoryName);

            if (empty($errors)) {
                $newValue = ['name' => $categoryName];

                CategoryStorage::update($this->model, $newValue);
                $url = Router::getUrl('/admin/categories');
                $this->redirect($url);
            }
        }

        return $this->render('admin/categories/edit', ['model' => $this->model, 'errors' => $errors]);
    }

    private function validate($categoryName)
    {
        $errors = [];

        if (mb_strlen($categoryName, 'UTF-8') < 3 || mb_strlen($categoryName, 'UTF-8') > 15) {
            $errors['name'][] = 'Category name must be between 3 and 15 letters!';
        }

        return $errors;
    }
}