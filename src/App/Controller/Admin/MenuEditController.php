<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 3.3.2019 г.
 * Time: 22:48
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Helper\CategoryList;
use ClubSoftware\Helper\ParamsHelper;
use ClubSoftware\Model\MenuItem;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\MenuItemStorage;
use ClubSoftware\Storage\SourceStorage;
use Intervention\Image\ImageManagerStatic as Image;

class MenuEditController extends LayoutController
{
    private $model;
    private $categoryList;
    private $sources;
    protected $layout = 'layouts/admin';
    protected $title = 'Edit Users';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->model = MenuItemStorage::get($id);
        $this->sources = SourceStorage::all();
        if (empty($this->model)) {
            return false;
        }
        $this->categoryList = new CategoryList($this->model->getCategoryId());

        return parent::preAction();
    }

    public function doAction()
    {
        $errors = [];

        if (!empty($_POST)) {
            $itemName = ParamsHelper::getPostParam('itemName', '');
            $description = ParamsHelper::getPostParam('description', '');
            $price = ParamsHelper::getPostParam('price', '');
            $source = ParamsHelper::getPostParam('sourceId', '');
            $categoryId = ParamsHelper::getPostParam('parentId', '');

            $errors = $this->validate($itemName, $description, $price, $source);

            if (empty($errors)) {
                $newValues = [
                    'itemName' => $itemName, 'description' => $description,
                    'price' => $price, 'source' => $source, 'category_id' => $categoryId
                ];

                $webPath = realpath(__DIR__.'/../../../../web');
                $imgPath = '/views/images/products';
                if (!empty($_FILES['image']['tmp_name'])) {
                    $tmpPath = $_FILES['image']['tmp_name'];
                    $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                    if (!in_array($extension, ['jpg', 'jpeg', 'png'])) {
                        $errors['image'][] = 'Wrong Extension!';
                    } else {
                        $fileName = $imgPath . '/' . $this->model->getId() . '.' . pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                        $img = Image::make($tmpPath);
                        $img->resize(255, 150);
                        $img->save($webPath . $fileName);
                        $newValues['img_path'] = $fileName;
                    }
                }

                MenuItemStorage::update($this->model, $newValues);
                $url = Router::getUrl('/admin/menu');
                $this->redirect($url);
            }
        }

        return $this->render('admin/menu/edit', [
            'model' => $this->model,
            'sources' => $this->sources,
            'categoryList' => $this->categoryList,
            'errors' => $errors
        ]);
    }

    private function validate($itemName, $description, $price, $source)
    {
        $errors = [];

        if (mb_strlen($itemName, 'UTF-8') < 3 || mb_strlen($itemName, 'UTF-8') > 20) {
            $errors['itemName'][] = 'Item name must be between 3 and 20 letters!';
        }

        if (mb_strlen($description, 'UTF-8') > 50) {
            $errors['description'][] = 'Description must be no more than 50 letters!';
        }

        if (empty($price)) {
            $errors['price'][] = 'Price Cannot Be Empty';
            return $errors;
        }

        if (empty($source)) {
            $errors['source'][] = 'Please select source!';
        }

        return $errors;
    }
}