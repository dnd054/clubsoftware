<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\OrderStorage;

/**
 * Class CurrentOrderController
 * @package App\AdminController
 * @Route admin/orders
 */
class OrdersController extends LayoutController
{
    private $orderId;
    protected $layout = 'layouts/admin';
    protected $title = 'Orders';

    public function doAction()
    {
        $orders = [];
        $errors = [];
        $limit = 10;
        $pager = new Pager($limit);

        if (!empty($_GET['id'])) {
            $this->orderId = $_GET['id'];
            $orders[] = OrderStorage::get($this->orderId);
            $errors = $this->validate($this->orderId, $orders);
        }

        if (!empty($errors) || empty($this->orderId)) {
            $query = (new Query())
                ->offset($pager->getOffset())
                ->limit($limit);
            $orders = OrderStorage::all($query);
            $rows = $pager->queryCount($query);
        }

        return $this->render('admin/orders/index', [
            'orders' => $orders,
            'pager' => $pager,
            'errors' => $errors
        ]);
    }

    private function validate($orderId, $orders)
    {
        $errors = [];

        if (!is_numeric($orderId) && !empty($orderId)) {
            $errors['orderId'][] = 'Order ID must be a number!';
        } elseif ($orderId < 1 || $orderId > 1000) {
            $errors['orderId'][] = 'Order ID must be between 1 and 1000!';
        } elseif (!$orders[0]) {
            $errors['orderId'][] = 'There is no such order!';
        }

        return $errors;
    }
}
