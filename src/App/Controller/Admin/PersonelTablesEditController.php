<?php

namespace ClubSoftware\App\Controller\Admin;

use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\UsersTablesStorage;
use ClubSoftware\Storage\PersonelTablesStorage;
use ClubSoftware\Storage\TableStorage;
use ClubSoftware\Storage\UserStorage;

class PersonelTablesEditController extends LayoutController
{
    private $userId;
    private $assignedTables = [];
    private $freeTables;
    private $personelName;
    protected $layout = 'layouts/admin';
    protected $title = 'Personel';

    public function preAction()
    {
        $id = $_GET['id'];
        $this->userId = $id;
        $this->personelName = UserStorage::get($id)->getUserName();
        $usersTables = PersonelTablesStorage::getUsersTables($id);
        foreach ($usersTables as $userTable) {
            $tableId = $userTable->getIdTable();
            $this->assignedTables[] = TableStorage::get($tableId);
        }
        $this->freeTables = PersonelTablesStorage::getFreeTables();

        return parent::preAction();
    }

    public function doAction()
    {
        $assignedTables = UsersTablesStorage::getCheckboxList($this->assignedTables, true);
        $freeTables = UsersTablesStorage::getCheckboxList($this->freeTables, false);
        $personelTables = array_merge($assignedTables, $freeTables);

        if (!empty($_POST['tableIds'])) {
            $tableIds = $_POST['tableIds'];
            PersonelTablesStorage::unAssignTables($this->userId);

            foreach ($tableIds as $tableId) {
                if (!in_array($tableId, array_values($this->assignedTables))) {
                    PersonelTablesStorage::assignTable($this->userId, $tableId);
                }
            }

            $url = Router::getUrl('/admin/personel');
            $this->redirect($url);
        }

        return $this->render('admin/personel/edit', [
            'personelTables' => $personelTables,
            'title' => $this->title,
            'personelName' => $this->personelName
            ]);
    }
}