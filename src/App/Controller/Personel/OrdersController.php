<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Personel;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\OrderDropdownStatusButton;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\OrderStorage;
use ClubSoftware\Storage\PersonelTablesStorage;
use ClubSoftware\Storage\UserStorage;

/**
 * Class CurrentOrderController
 * @package App\PersonelController
 * @Route personel/orders
 */
class OrdersController extends LayoutController
{
    private $tableIds;
    private $personelName;
    protected $layout = 'layouts/personel';
    protected $title = 'Orders';

    public function preAction()
    {
        $personelId = $_SESSION['user']->getId();
        $this->personelName = UserStorage::get($personelId)->getUserName();
        $query = (new Query())
            ->select('id_table')
            ->where('id_user', $personelId)
            ;
        $this->tableIds = PersonelTablesStorage::all($query);

        return parent::preAction();
    }

    public function doAction()
    {
        $rawOrders = '';
        $tableIds = $this->tableIds;
        $limit = 10;
        $pager = new Pager($limit);

        foreach ($tableIds as $tableId) {
            $query = (new Query())
                ->where('table_id', $tableId->getIdTable())
                ->orderBy('id DESC')
                ->offset($pager->getOffset())
                ->limit($limit);
            if (OrderStorage::all($query)) {
                $rawOrders = OrderStorage::all($query);
            }
            $rows = $pager->queryCount($query);
        }

        $orders = $this->makeTable($rawOrders);

        return $this->render('personel/orders/index', [
            'personelName' => $this->personelName,
            'orders' => $orders,
            'pager' => $pager
        ]);
    }

    private function makeTable($rawOrders)
    {
        $statusButton = new OrderDropdownStatusButton();
        $orders = [];

        foreach ($rawOrders as $order) {
            $row = '<tr>';
            $row .= '<td id="fixed-id">'.$order->getId().'</td>';
            $row .= '<td>'.$order->getTable()->getName().'</td>';
            $row .= '<td id="fixed-actions">'.$statusButton->getButton($order->getStatus(), $order->getId()).'</td>';
            $row .= '<td><a href="'. Router::getUrl('/personel/orderItems', ['orderId' => $order->getId()]).'" title="View Record">';
            $row .= '<span id="glyphs"><i class="fas fa-eye"></i></span></a></td>';
            $row .= '</tr>';

            $orders[] = $row;
        }

        return $orders;
    }
}
