<?php

/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:44
 */

namespace ClubSoftware\App\Controller\Personel;

use ClubSoftware\Db\Query;
use ClubSoftware\Helper\OrderItemDropdownStatusButton;
use ClubSoftware\Helper\Pager;
use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Storage\OrderItemStorage;
use ClubSoftware\Storage\OrderStorage;
use ClubSoftware\Storage\SourceStorage;

/**
 * Class OrderItemsController
 * @package App\PersonelController
 * @Route personel/orders/orderItems
 */
class OrderItemsController extends LayoutController
{
    protected $orderId;
    protected $layout = 'layouts/personel';
    protected $title = 'Order Items';

    public function preAction()
    {
        $id = $_GET['orderId'];
        $this->orderId = $id;

        return parent::preAction();
    }

    public function doAction()
    {
        $limit = 10;
        $pager = new Pager($limit);
        $order = OrderStorage::get($this->orderId);

        $query = (new Query())
            ->where('order_id', $this->orderId)
            ->orderBy('id DESC')
            ->offset($pager->getOffset())
            ->limit($limit);

        $rawOrderItems = OrderItemStorage::all($query);
        $orderItems = $this->makeTable($rawOrderItems);
        $rows = $pager->queryCount($query);

        return $this->render('personel/orders/orderItems/index', [
            'order' => $order,
            'orderItems' => $orderItems,
            'pager' => $pager
        ]);
    }

    private function makeTable($rawOrderItems)
    {
        $statusButton = new OrderItemDropdownStatusButton();
        $orderItems = [];

        foreach ($rawOrderItems as $orderItem) {
            $sourceId = $orderItem->getMenuItem()->getSourceId();
            $sourceName = SourceStorage::get($sourceId)->getName();
            $row = '<tr>';
            $row .= '<td id="fixed-id">'.$orderItem->getId().'</td>';
            $row .= '<td>'.$orderItem->getMenuItem()->getName().'</td>';
            $row .= '<td>'.$sourceName.'</td>';
            $row .= '<td id="fixed-actions">'.$statusButton->getButton($orderItem->getStatus(), $orderItem->getId()).'</td>';
            $row .= '</tr>';

            $orderItems[] = $row;
        }

        return $orderItems;
    }
}
