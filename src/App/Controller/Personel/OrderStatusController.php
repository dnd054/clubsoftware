<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 26.4.2019 г.
 * Time: 14:14
 */

namespace ClubSoftware\App\Controller\Personel;

use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;
use ClubSoftware\Storage\OrderStorage;

class OrderStatusController extends LayoutController
{
    private $model;
    private $statusId;

    public function preAction()
    {
        $orderId = $_GET['orderId'];
        $this->statusId = $_GET['statusId'];
        $this->model = OrderStorage::get($orderId);

        if (empty($this->model)) {
            return false;
        }

        return parent::preAction();
    }
    public function doAction()
    {
        $newValue = ['status' => $this->statusId];

        OrderStorage::update($this->model, $newValue);
        $url = Router::getUrl('/personel/orders');
        $this->redirect($url);
    }
}
