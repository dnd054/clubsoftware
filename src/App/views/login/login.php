<div class="login-wrapper">
    <h2 class="text-primary">Login</h2>
    <p class="text-danger">Please fill in your credentials to login.</p>
    <form action="" method="post">
        <div class="form-group">
            <label for="username"><h5 class="text-dark">Username</h5></label>
            <input type="text" name="username" id="username" class="form-control" value="">
            <span class="help-block"></span>
            <?php if (!empty($errors['login'])) :?>
                <?php foreach ($errors['login'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="password"><h5 class="text-dark">Password</h5></label>
            <input type="password" name="password" id="password" class="form-control">
            <span class="help-block"></span>
        </div>
        <div class="btn-group float-right">
            <button class="btn btn-primary" type="submit" id="users">
                <i class="far fa-user"></i> Login
            </button>
        </div>
    </form>
</div>