<div class="action-wrapper">
    <h3 class="text-primary">Add New Table</h3>
    <p class="text-danger">Please fill out this form to set table.</p>
    <form action="" method="post">
        <div class="form-group">
            <label>Table Name</label>
            <input type="text" name="tableName" class="form-control" value="">
            <?php if (!empty($errors['tableName'])) :?>
                <?php foreach ($errors['tableName'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>