<div class="action-wrapper">
    <h3 class="text-primary">Change Source Name</h3>
    <p class="text-danger">Please fill out this form to reset source name.</p>
    <form action="" method="post">
        <div class="form-group">
            <label>Source Name</label>
            <input type="text" name="name" class="form-control" value="<?= $model->getName() ?>">
            <?php if (!empty($errors['name'])) :?>
                <?php foreach ($errors['name'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Source Users</label>
            <select name="userId" class="form-control">
                <option value="">Select User ...</option>
                <?php foreach ($sourceUsers as $sourceUser) {?>
                    <option value="<?= $sourceUser->getId() ?>"
                        <?= $sourceUser->getId() == $model->getUserId() ? ' selected' : '' ?>
                    ><?= $sourceUser->getUserName() ?></option>
                <?php }?>
            </select>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>