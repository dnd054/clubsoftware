<div class="action-wrapper">
    <h3 class="text-primary">Add New Source</h3>
    <p class="text-danger">Please fill out this form to set source.</p>
    <form action="" method="post">
        <div class="form-group">
            <label>Source Name</label>
            <input type="text" name="name" class="form-control" value="">
            <?php if (!empty($errors['name'])) :?>
                <?php foreach ($errors['name'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Source Users</label>
            <select name="userId" class="form-control">
                <option value="">Select User ...</option>
                <?php foreach ($sourceUsers as $sourceUser) {?>
                <option value="<?= $sourceUser->getId() ?>"><?= $sourceUser->getUserName() ?></option>
                <?php }?>
            </select>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>