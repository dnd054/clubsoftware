<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
        pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $title ?></h1>
    <div class="row float-right p-0">
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/welcome'); ?>">Home</a>
        <p>-></p>
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<div class="table-responsive section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                    <div class="float-left ml-3">
                        <form action="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/orders'); ?>id=" method="get" class="float-left">
                            <div class="row float-left" id="no-space">
                                <div class="input-group mb-3">
                                    <input type="number" name="id" class="form-control float-right" min="1" max="999" placeholder="ID">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default">Enter</button>
                                    </span>
                                </div>
                            </div>
                            <div class="float-right ml-4 mt-2">
                            <?php if (!empty($errors['orderId'])) :?>
                                <?php foreach ($errors['orderId'] as $error) :?>
                                    <p class="text-danger"><?= $error ?></p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </form>
                    </div>
                    <tr>
                        <th id="fixed-id">#</th>
                        <th>Table</th>
                        <th>Status</th>
                        <th id="fixed-actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orders as $order) {?>
                        <tr>
                            <?php foreach ($order->getOutputFields() as $field => $stringRenderer) { ?>
                                <td><?php echo $stringRenderer($order) ?></td>
                            <?php } ?>
                            <td>
                                <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/orders/orderItems', ['id' => $order->getId()]); ?>"
                                   title="View Record">
                                    <span id="glyphs"><i class="fas fa-eye"></i></span></a>
                                <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/orders/edit', ['id' => $order->getId()]); ?>"
                                   title="Update Record">
                                    <span id="glyphs"><i class="fas fa-pencil-alt"></i></span></a>
                                <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/orders/delete', ['id' => $order->getId()]); ?>"
                                   title="Delete Record"
                                   onclick="return confirm('Are you sure?');">
                                    <span id="glyphs"><i class="fa fa-trash"></i></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pager ?>
    </div>
</div>
