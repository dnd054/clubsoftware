<div class="action-wrapper">
    <h3 class="text-primary">Change Order</h3>
    <p class="text-danger">Please fill out this form to reset order.</p>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Order ID</label>
            <input name="orderId" class="form-control" value="<?= $model->getId() ?>" readonly>
        </div>
        <div class="form-group">
            <label>Tables</label>
            <select name="tableId" class="form-control">
                <option value="">Select Table ...</option>
                <?php foreach ($tables as $table) {?>
                    <option value="<?= $table->getId() ?>"
                        <?= $table->getId() == $model->getTable()->getId() ? ' selected' : '' ?>
                    ><?= $table->getName() ?></option>
                <?php }?>
            </select>
        </div>
        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
                <option value="<?= $model->getStatus() ?>">Select Status ...</option>
                <option value="<?= \ClubSoftware\Model\Order::STATUS_NEW ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\Order::STATUS_NEW ? ' selected' : '' ?>>
                    New</option>
                <option value="<?= \ClubSoftware\Model\Order::STATUS_PROGRESS ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\Order::STATUS_PROGRESS ? ' selected' : '' ?>>
                    Progress</option>
                <option value="<?= \ClubSoftware\Model\Order::STATUS_FINISHED ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\Order::STATUS_FINISHED ? ' selected' : '' ?>>
                    Finished</option>
            </select>
            <?php if (!empty($errors['status'])) :?>
                <?php foreach ($errors['status'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>