<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
    pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $title ?></h1>
    <div class="row float-right">
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/welcome') ?>">Home</a>
        <p>-></p>
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/orders') ?>">Orders</a>
        <p>-></p>
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/orders/orderItems/add', ['orderId' => $order->getId()]); ?>" class="btn btn-success float-right mt-3">
    Add New Item</a>
<div class="table-responsive section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="order-data">&nbsp;
                    <?php foreach ($order->getOutputFields() as $field => $stringRenderer) { ?>
                        <?= $field . ' -> ' . $stringRenderer($order) . '&nbsp;' . '| '?>
                    <?php } ?>
                </div>
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th id="fixed-id">#</th>
                        <th>Name</th>
                        <th>Source</th>
                        <th>Status</th>
                        <th id="fixed-actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $orderItems = $order->getOrderItems();
                    ?>
                    <?php foreach ($orderItems as $orderItem) {?>
                        <tr>
                            <?php foreach ($orderItem->getOutputFields() as $field => $stringRenderer) {?>
                                <td><?php echo $stringRenderer($orderItem) ?></td>
                            <?php }?>
                            <td>
                                <a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/orders/orderItems/edit', ['id' => $orderItem->getId()]); ?>"
                                   title="Update Record">
                                    <span id="glyphs"><i class="fas fa-pencil-alt"></i></span></a>
                                <a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/orders/orderItems/delete', ['id' => $orderItem->getId()]); ?>"
                                   title="Delete Record"
                                   onclick="return confirm('Are you sure?');">
                                    <span id="glyphs"><i class="fa fa-trash"></i></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>