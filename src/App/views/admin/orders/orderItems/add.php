<div class="action-wrapper">
    <h3 class="text-primary">Add Order Item</h3>
    <p class="text-danger">Please fill out this form to add order item.</p>
    <form action="" method="post">
        <div class="form-group">
            <label>Menu Items</label>
            <select multiple class="form-control" name="menuItemId">
                <option class="bg-light">Select Menu Item ...</option>
                <?php foreach ($menuItems as $menuItem) {?>
                    <option value="<?= $menuItem->getId() ?>"><?= $menuItem->getName() ?></option>
                <?php }?>
            </select>
            <?php if (!empty($errors['menuItemId'])) :?>
                <?php foreach ($errors['menuItemId'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <div class="form-group">
                <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                    <i class="fa fa-paper-plane"></i>
                    <span class="text-white">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>