<div class="action-wrapper">
    <h3 class="text-primary">Change Order Items</h3>
    <p class="text-danger">Please fill out this form to reset order item.</p>
    <form action="" method="post">
        <div class="form-group">
            <label>Menu Item</label>
            <input class="form-control" value="<?= $model->getMenuItem()->getName() ?>" readonly>
        </div>
        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
                <option value="<?= $model->getStatus() ?>">Select Status ...</option>
                <option value="<?= \ClubSoftware\Model\OrderItem::STATUS_NEW ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\OrderItem::STATUS_NEW ? ' selected' : '' ?>>
                    New</option>
                <option value="<?= \ClubSoftware\Model\OrderItem::STATUS_PROGRESS ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\OrderItem::STATUS_PROGRESS ? ' selected' : '' ?>>
                    Progress</option>
                <option value="<?= \ClubSoftware\Model\OrderItem::STATUS_READY ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\OrderItem::STATUS_READY ? ' selected' : '' ?>>
                    Ready</option>
                <option value="<?= \ClubSoftware\Model\OrderItem::STATUS_SERVED ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\OrderItem::STATUS_SERVED ? ' selected' : '' ?>>
                    Served</option>
                <option value="<?= \ClubSoftware\Model\OrderItem::STATUS_PAYED ?>"
                    <?= $model->getStatus() == \ClubSoftware\Model\OrderItem::STATUS_PAYED ? ' selected' : '' ?>>
                    Payed</option>
            </select>
            <?php if (!empty($errors['status'])) :?>
                <?php foreach ($errors['status'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>