<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
    pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $title ?></h1>
    <div class="row float-right">
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/welcome') ?>">Home</a>
        <p>-></p>
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<div class="action-wrapper">
    <h3 class="text-primary">Change Category Name</h3>
    <p class="text-danger">Please enter new category name.</p>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Category Name</label>
            <input type="text" name="name" class="form-control" value="<?= $model->getName() ?>">
            <?php if (!empty($errors['name'])) :?>
                <?php foreach ($errors['name'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <div class="form-group">
                <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                    <i class="fa fa-paper-plane"></i>
                    <span class="text-white">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>
