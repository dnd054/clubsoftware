<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
    pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $title ?></h1>
    <div class="row float-right">
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/welcome') ?>">Home</a>
        <p>-></p>
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<div class="table-responsive section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                    <a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/categories/add'); ?>"
                       class="btn btn-success float-right mb-3">
                        Add New Category</a>
                    <tr>
                        <th id="fixed-id">#</th>
                        <th>Name</th>
                        <th>Path</th>
                        <th id="fixed-actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($categories as $category) {?>
                        <tr>
                            <td><?= $category->getId()?></td>
                            <td><?= $category->getName()?></td>
                            <td><?= \ClubSoftware\Helper\CategoriesHelper::getPath($category) ?></td>
                            <td>
                                <a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/categories/edit', ['id' => $category->getId()]); ?>"
                                   title="Update Record">
                                    <span id="glyphs"><i class="fas fa-pencil-alt"></i></span></a>
                                <a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/categories/delete', [
                                        'id' => $category->getId()
                                ]); ?>"
                                   title="Delete Record"
                                   onclick="return confirm('Are you sure?');">
                                    <span id="glyphs"><i class="fa fa-trash"></i></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pager ?>
    </div>
</div>
