<div class="action-wrapper">
    <h3 class="text-primary">Add Menu Item</h3>
    <p class="text-danger">Please fill out this form to set menu item.</p>
    <form action="" method="post" enctype="multipart/form-data">
        <label>Image</label>
        <div class="form-group mb-3">
            <div>
                <i class="fa fa-file-image"></i>
                <span class="text-info">Image Name!</span>
            </div>
            <div class="input-group">
                <input type="text" class="form-control" value="Select Image ..." readonly>
                <span class="input-group-btn">
                    <span class="btn btn-secondary btn-file">
                    <i class="fa fa-folder-open"></i>
                        Browse<input type="file" name="image">
                    </span>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="itemName" class="form-control" value="">
            <?php if (!empty($errors['itemName'])) :?>
                <?php foreach ($errors['itemName'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" name="description" rows="2" value=""></textarea>
            <?php if (!empty($errors['description'])) :?>
                <?php foreach ($errors['description'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" class="form-control" value="">
            <?php if (!empty($errors['price'])) :?>
                <?php foreach ($errors['price'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Sources</label>
            <select name="sourceId" class="form-control">
                <option value="">Select Source ...</option>
                <?php foreach ($sources as $source) {?>
                    <option value="<?= $source->getId() ?>"><?= $source->getName() ?></option>
                <?php }?>
            </select>
            <?php if (!empty($errors['source'])) :?>
                <?php foreach ($errors['source'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Categories</label>
            <select multiple class="form-control" name="parentId">
                <option value="" selected>Select Category</option>
                <?= $categoryList ?>
            </select>
        </div>
        <div class="form-group">
            <div class="form-group">
                <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                    <i class="fa fa-paper-plane"></i>
                    <span class="text-white">Submit</span>
                </button>
            </div>
        </div>
    </form>
</div>