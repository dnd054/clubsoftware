<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
    pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $title ?></h1>
    <div class="row float-right">
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/welcome') ?>">Home</a>
        <p>-></p>
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<div class="table-responsive section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th id="fixed-id">#</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th id="fixed-time">Last Active</th>
                        <th id="fixed-quantity">Status</th>
                        <th id="fixed-actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($personel as $person) {?>
                        <tr>
                            <?php foreach ($person->getOutputFields() as $field => $stringRenderer) { ?>
                                <td><?php echo $stringRenderer($person) ?></td>
                            <?php } ?>
                            <td>
                                <a href="<?php echo \ClubSoftware\Mvc\Router::getUrl('/admin/personel/edit', ['id' => $person->getId()]); ?>"
                                   title="Update Record">
                                    <span id="glyphs"><i class="fas fa-pencil-alt"></i></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pager ?>
    </div>
</div>
