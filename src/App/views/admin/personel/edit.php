<div class="row float-right mr-1">
    <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/welcome') ?>">Home</a>
    <p>-></p>
    <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/personel') ?>"><?= $title ?></a>
    <p>-></p>
    <a class="text-primary" href="#"><?= $personelName ?></a>
</div>
<div class="action-wrapper">
    <h3 class="text-primary">Change <?= $personelName ?>'s Tables</h3>
    <p class="text-danger">Please fill out this form to reset <?= $personelName ?>'s tables.</p>
    <form action="" method="post" class="no-gutters" enctype="multipart/form-data">
        <div class="col-md-12">
            <div class="cards" id="checkbox">
                <div class="card-header"><?= $personelName ?>'s Tables</div>
                <ul class="list-group list-group-flush">
                <?php foreach ($personelTables as $personelTable) {?>
                    <?= $personelTable ?>
                <?php }?>
                </ul>
            </div>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mt-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>