<div class="action-wrapper">
    <h3 class="text-primary">Change User Credentials</h3>
    <p class="text-danger">Please fill out this form to reset credentials.</p>
    <form action="" method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?= $model->getUsername() ?>">
            <?php if (!empty($errors['username'])) :?>
                <?php foreach ($errors['username'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Role</label>
            <select name="role" class="form-control">
                <option value="">Select Role ...</option>
                <option value="<?= \ClubSoftware\Model\User::TYPE_CLIENT ?>"
                <?= $model->getRole() == \ClubSoftware\Model\User::TYPE_CLIENT ? ' selected' : '' ?>>Client</option>
                <option value="<?= \ClubSoftware\Model\User::TYPE_ADMIN ?>"
                    <?= $model->getRole() == \ClubSoftware\Model\User::TYPE_ADMIN ? ' selected' : '' ?>>Admin</option>
                <option value="<?= \ClubSoftware\Model\User::TYPE_SOURCE ?>"
                    <?= $model->getRole() == \ClubSoftware\Model\User::TYPE_SOURCE ? ' selected' : '' ?>>Source</option>
                <option value="<?= \ClubSoftware\Model\User::TYPE_PERSONEL ?>"
                    <?= $model->getRole() == \ClubSoftware\Model\User::TYPE_PERSONEL ? ' selected' : '' ?>>Personel</option>
            </select>
            <?php if (!empty($errors['role'])) :?>
                <?php foreach ($errors['role'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>New Password</label>
            <input type="password" name="new_password" class="form-control">
            <span class="help-block"></span>
            <?php if (!empty($errors['new_password'])) :?>
                <?php foreach ($errors['new_password'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label>Confirm New Password</label>
            <input type="password" name="confirm_password" class="form-control">
            <span class="help-block"></span>
            <?php if (!empty($errors['confirm_password'])) :?>
                <?php foreach ($errors['confirm_password'] as $error) :?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <button  class="btn btn-primary float-right mb-3" type="submit" title="Submit" value="Submit">
                <i class="fa fa-paper-plane"></i>
                <span class="text-white">Submit</span>
            </button>
        </div>
    </form>
</div>