<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="/resources/css/main.css">
    <link rel="stylesheet" href="/resources/css/tabs.css">
    <link rel="stylesheet" href="/resources/css/dropdown-menu.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/resources/fontawesome/css/all.css">
    <link rel="stylesheet" href="/resources/css/nav-tabs.css">
    <script src="/resources/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="/resources/js/dropdown-menu.js" type="text/javascript"></script>
    <script src="/resources/bootstrap/js/bootstrap.js"></script>
    <script src="/resources/js/backToTop.js"></script>
    <title>ClubSoftware <?= $title ?> </title>
</head>

<body>
<nav class="navbar navbar-dark bg-dark flex-md-nowrap p-0 shadow fixed-top">
    <a class="navbar-brand pt-0" href="#"><img src="/views/images/logos/logo.png"></a>
    <div class="col">
        <a class="toggle-nav" data-toggle="collapse" data-target="#sidebarCollapse"
           id="sidebarMargin" href="#" title="Toggle Menu" role="button"><i class="fas fa-bars">&nbsp;Menu</i></a>
        <a href="#" title="Back To Top" class="back-to-top ml-4">
            <i class="fa fa-arrow-circle-up"></i></a>
    </div>
    <a class="btn btn-danger text-white mr-2" href="<?= \ClubSoftware\Mvc\Router::getUrl('logout') ?>">Sign Out</a>
</nav>
<div class="container-fluid">
    <div class="row">
        <aside class="col-12 col-md-2 p-0 bg-dark sidebar collapse show" id="sidebarCollapse">
            <div class="content">
                <div id="jquery-accordion-menu" class="jquery-accordion-menu sidebar-sticky">
                    <ul>
                        <?= $categoryTree ?>
                    </ul>
                </div>
            </div>
        </aside>
        <main class="col px-0" id="client">
            <nav class="border-bottom border-primary w-100">
                <div class="nav nav-tabs" id="tabs" role="tablist">
                <?= $tabs; ?>
                </div>
            </nav>
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="my-1">
                                <?= $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarMargin').on('click', function () {
            $('#client').toggleClass('active');
        });
    });
</script>
</body>
</html>