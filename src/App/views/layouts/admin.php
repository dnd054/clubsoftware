<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="/resources/css/main.css">
    <link rel="stylesheet" href="/resources/css/dashboard.css">
    <link rel="stylesheet" href="/resources/css/checkbox.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/resources/fontawesome/css/all.css">
    <script src="/resources/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="/resources/bootstrap/js/bootstrap.js"></script>
    <script src="/resources/js/backToTop.js"></script>
    <script src="/resources/js/chooseFile.js"></script>
    <title>ClubSoftware <?= $title ?></title>
</head>

<body class="bg-light">
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand pt-0" href="#"><img src="/views/images/logos/logo.png"></a>
    <div class="col">
        <a class="toggle-nav" data-toggle="collapse" data-target="#sidebarCollapse"
           id="sidebarMargin" href="#" title="Toggle Menu" role="button"><i class="fas fa-bars"></i></a>
        <a href="#" title="Back To Top" class="back-to-top ml-4">
            <i class="fa fa-arrow-circle-up"></i></a>
    </div>
    <a class="btn btn-danger text-white mr-2" href="<?= \ClubSoftware\Mvc\Router::getUrl('logout') ?>">Sign Out</a>
</nav>

<div class="container-fluid">
    <div class="row">
        <aside class="col-12 col-md-2 p-0 bg-dark sidebar collapse show" id="sidebarCollapse">
            <nav class="navbar navbar-expand navbar-dark bg-dark align-items-start" id="sidebar-ht">
                <div class="navbar-collapse sidebar-sticky mtop">
                    <ul class="w-100 mr-4 justify-content-between fa-ul menu">
                        <li class="border-bottom border-primary p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                            <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/menu') ?>">Menu</a>
                        </li>
                        <li class="border-bottom border-primary p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                            <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/users') ?>">Users</a>
                        </li>
                        <li class="border-bottom border-primary p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                            <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/tables') ?>">Tables</a>
                        </li>
                        <li class="border-bottom border-primary p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                            <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/orders') ?>">Orders</a>
                        </li>
                        <li class="border-bottom border-primary p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                        <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/sources') ?>">Sources</a>
                        </li>
                        <li class="border-bottom border-primary p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                        <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/personel') ?>">Personel</a>
                        </li>
                        <li class="p-2"><span class="fa-li" ><i class="fa fa-wrench"></i></span>
                            <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/admin/categories') ?>">Categories</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </aside>
        <main class="col" id="admin">
            <?= $content; ?>
        </main>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('#sidebarMargin').on('click', function () {
            $('#admin').toggleClass('active');
        });

    });
</script>
</body>
</html>