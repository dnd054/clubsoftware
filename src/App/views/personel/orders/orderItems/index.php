<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
    pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $title ?></h1>
    <div class="row float-right">
        <a class="text-primary" href="<?= \ClubSoftware\Mvc\Router::getUrl('/personel/orders') ?>">Orders</a>
        <p>-></p>
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<div class="table-responsive section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="order-data">&nbsp;&nbsp;Order&nbsp;->&nbsp;|
                    <?php foreach ($order->getOutputFields() as $field => $stringRenderer) { ?>
                        <?= $field . ' -> ' . $stringRenderer($order) . '&nbsp;' . '| '?>
                    <?php } ?>
                </div>
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th id="fixed-id">#</th>
                        <th>Name</th>
                        <th>Source</th>
                        <th id="fixed-actions">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orderItems as $orderItem) {?>
                        <?= $orderItem ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>