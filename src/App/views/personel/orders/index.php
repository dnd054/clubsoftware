<div class="container-fluid d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center
        pb-2 border-bottom border-primary">
    <h1 class="h2 text-primary"><?= $personelName.'\'s '.$title ?></h1>
    <div class="row float-right p-0">
        <a class="text-primary" href="#"><?= $title ?></a>
    </div>
</div>
<div class="table-responsive section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th id="fixed-id">#</th>
                        <th>Table</th>
                        <th id="fixed-actions">Status</th>
                        <th id="fixed-actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orders as $order) {?>
                        <?= $order ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pager ?>
    </div>
</div>
