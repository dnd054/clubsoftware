<?php if (!empty($orders)) {?>
    <?php foreach ($orders as $key => $order) {?>
        <?= $ordersTables[$key] ?>
    <?php }?>
    <div class="float-right mt-3">
        <button  class="btn btn-primary" title="New Order">
            <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/client/menu') ?>" class="text-white">New Order</a>
        </button>
    </div>
<?php } else {?>
    <p>You haven't payed an order yet!</p>
<?php }?>

