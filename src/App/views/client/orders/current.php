<?php if (!empty($currentOrder)) {?>
    <?= $orderTable ?>
    <button class="btn btn-primary float-right mt-3" title="Pay">
        <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/client/orders/pay') ?>">
            <i class="fa fa-credit-card text-white"></i><span class="text-white">&nbsp;Pay</span></a>
    </button>
<?php } else {?>
<p>You haven't ordered anything yet!</p>
<?php }?>




