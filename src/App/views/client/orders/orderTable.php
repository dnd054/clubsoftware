<div class="mt-3" id="order-data">&nbsp;
    <?php foreach ($order->getOutputFields() as $field => $stringRenderer) { ?>
        <?= $field . ' -> ' . $stringRenderer($order) . '&nbsp;' . '| '?>
    <?php } ?>
</div>
<table class="table table-bordered table-hover bg-light">
    <thead class="thead-dark">
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th id="fixed-quantity">Quantity</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody class="table table-bordered table-hover">
    <?php
    $total = 0.0;

    foreach ($orderItems as $item) {
        $price = $item['price'];
        $total += ($price * $item['quantity']);
        ?>
        <tr>
            <td><?= $item['name'] ?></td>
            <td><?= $item['description'] ?></td>
            <td><?= $item['quantity'] ?></td>
            <td><?= $price ?></td>
        </tr>
    <?php }?>
    </tbody>
</table>
<div class="" id="total">Total: <?= number_format((float)$total, 2, '.', '') ?> lv.</div>
