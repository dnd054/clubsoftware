<?php if (!empty($basketItems)) {?>
<table class="table table-bordered table-hover mt-3 bg-light">
    <thead class="thead-dark">
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th id="fixed-quantity">Quantity</th>
        <th>Price</th>
        <th id="fixed-actions">Actions</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $total = 0.0;

        foreach ($basketItems as $item) {
                $price = $item['price'];
                $total += ($price * $item['quantity']);
            ?>
            <tr>
                <td><?= $item['name'] ?></td>
                <td><?= $item['description'] ?></td>
                <td><input name="quantity" type="number" min="1" max="99" class="form-control" placeholder="QTY"
                    value="<?= $item['quantity'] ?>">
                </td>
                <td><?= $price ?></td>
                <td>
                    <a href="<?= \ClubSoftware\Mvc\Router::getUrl('/client/basket/delete', ['productId' => $item['id']]); ?>"
                       title="Delete Record"
                       onclick="return confirm('Are you sure?');"
                        <?= \ClubSoftware\Mvc\Router::getUrl('/client/basket'); ?>>
                        <span id="glyphs"><i class="fa fa-trash"></i></span></a>
                </td>
            </tr>
        <?php }?>
    </tbody>
</table>
<div class="" id="total">Total: <?= number_format((float)$total, 2, '.', '') ?> lv.</div>
<button class="btn btn-success float-right mt-3" title="Order">
<a href="<?= \ClubSoftware\Mvc\Router::getUrl('/client/basket/order') ?>"><i class="fa fa-check text-white"></i>
    <span class="text-white">&nbsp;Order</span></a>
</button>
<?php } else {?>
<p>Your basket is empty!</p>
<?php }?>
