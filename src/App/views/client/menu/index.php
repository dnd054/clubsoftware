<div class="container-fluid">
    <div class="row justify-content-center no-gutters">
        <?php
        foreach ($menuItems as $menuItem) {?>
        <form action="<?= $_SERVER['REQUEST_URI'] ?>&productId=<?= $menuItem->getId(); ?>" method="post" enctype="multipart/form-data" class="no-gutters">
            <div class="col-md-4 col-sm-6 m-2 border border-primary bg-white" id="menu-item">
                <div class="row no-gutters">
                    <div class="col-12">
                        <img src="<?= $menuItem->getImgPath() ?>" id="product">
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold text-primary ml-2">
                            <u><?= $menuItem->getName() ?></u>
                        </div>
                        <div class="font-italic text-secondary ml-2">
                            <?= $menuItem->getDescription() ?>
                        </div>
                    </div>
                    <div class="col-12" id="bottom">
                        <div class="float-left text-danger ml-2 mt-2">
                            <span>Price: <?= $menuItem->getPrice() ?> lv.</span>
                        </div>
                        <div class="input-group line w-50 float-right">
                            <input name="quantity" type="number" min="1" max="99" class="form-control" placeholder="QTY">
                            <span class="input-group-btn">
                                <button type="submit" title="Add To Order" class="btn btn-success">
                                    <i class="fas fa-check text-white"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
    <?= $pager ?>
</div>
