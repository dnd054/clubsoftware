<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.3.2019 г.
 * Time: 10:42
 */

namespace ClubSoftware\App;

use ClubSoftware\Model\MenuItem;

class Basket
{
    const SESSION_KEY = 'orderItems';

    public function add($menuItemId, $quantity)
    {
        for ($i = 0; $i < $quantity; $i++) {
            $orderItem = new MenuItem();
            $orderItem->setId($menuItemId);
            $_SESSION[self::SESSION_KEY][] = $orderItem;
        }
    }

    public function getOrderItems()
    {
        if (!empty($_SESSION[self::SESSION_KEY])) {
            return $_SESSION[self::SESSION_KEY];
        }

        return [];
    }

    public function remove($productId)
    {
        foreach ($_SESSION[self::SESSION_KEY] as $key => $item) {
            if ($item->getId() == $productId) {
                unset($_SESSION[self::SESSION_KEY][$key]);
            }
        }
    }
}