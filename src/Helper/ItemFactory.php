<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 4.1.2019 г.
 * Time: 12:58
 */

namespace ClubSoftware\Helper;

class ItemFactory
{
    public static function build($class, $arrayToObject)
    {
        $object = new $class;

        foreach ($arrayToObject as $key => $value) {
            $setter = ItemHelper::getSetterName($key);

            if (method_exists($object, $setter)) {
                $object->$setter($value);
            }
        }

        return $object;
    }
}