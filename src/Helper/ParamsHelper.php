<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 4.3.2019 г.
 * Time: 9:42
 */

namespace ClubSoftware\Helper;

class ParamsHelper
{
    public static function getQueryParam($name, $default = null)
    {
        if (isset($_GET[$name])) {
            return $_GET[$name];
        }

        return $default;
    }
    public static function getPostParam($name, $default = null)
    {
        if (isset($_POST[$name])) {
            return $_POST[$name];
        }

        return $default;
    }
}