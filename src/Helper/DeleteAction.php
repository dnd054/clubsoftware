<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 9.3.2019 г.
 * Time: 23:25
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Mvc\LayoutController;
use ClubSoftware\Mvc\Router;

abstract class DeleteAction extends LayoutController
{
    public function doAction()
    {
        $key = $_GET['id'];

        $this->storage::delete($key);

        $url = Router::getUrl($this->redirectPath);
        $this->redirect($url);
    }
}