<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 9.3.2019 г.
 * Time: 15:00
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Db\Query;
use ClubSoftware\Storage\ItemStorage;

class Pager
{
    private $currentPage;
    private $perPage;
    private $total;

    public function __construct($perPage = 10)
    {
        $this->perPage = $perPage;
        $this->init();
    }

    public function init()
    {
        $page = 1;

        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        }
        $this->currentPage = $page;
    }

    public function getOffset()
    {
        $offset = (($this->currentPage - 1) * $this->perPage);
        return $offset;
    }

    public function queryCount(Query $query)
    {
        $countQuery = clone $query;
        $countQuery->limit(null);
        $countQuery->setSelectFields(['COUNT(*) AS count']);
        $count = ItemStorage::rawQuery($countQuery);
        $rows = $count[0]['count'];
        $this->total = $rows;

        return $rows;
    }

    public function getPageCount()
    {
        $pageCount = ceil($this->total / $this->perPage);

        return $pageCount;
    }

    public function getPages()
    {
        $pages = range(1, $this->getPageCount());

        return $pages;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function __toString()
    {
        $first = ($this->getOffset() + 1);
        $last = ($this->getOffset() + $this->perPage);

        $htmlPager = '<div class="clearfix float-right">';
        $htmlPager .= '<ul class="pagination">';
        $htmlPager .= '<li class="page-item"><a href="?page=1">First</a></li>';

        foreach ($this->getPages() as $page) {
            if ($page == $this->getCurrentPage()) {
                $htmlPager .= '<li class="page-item active"><a href="?page='.$page.'" class="page-link">'.$page.'</a></li>';
            } else {
                $htmlPager .= '<li class="page-item"><a href="?page='.$page.'" class="page-link">'.$page.'</a></li>';
            }
        }
        $htmlPager .= '<li class="page-item"><a href="?page='.$this->getPageCount().'" class="page-link">Last</a></li>';
        $htmlPager .= '</ul>';
        $htmlPager .= '</div>';
        $htmlPager .= '<div class="hint-text text-dark">Showing records from <strong>'.$first.'</strong> to <strong>'.$last.'</strong> out of <strong>'.$this->total.'</strong></div>';

        return $htmlPager;
    }
}