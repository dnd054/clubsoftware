<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 19.4.2019 г.
 * Time: 13:36
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Model\User;
use ClubSoftware\Storage\UserStorage;

abstract class SessionHandler
{
    public static function checkSessionExpired($lastActive)
    {
        $timeNow = strtotime(date('Y-m-d H:i:s'));
        $lastActive = strtotime($lastActive);
        $diff = abs(($timeNow - $lastActive) / 60);

        if ($diff > 30) {
            return true;
        }

        return false;
    }

    public static function updateSession()
    {
        $lifetime = 1800;
        $newValues['last_active'] = date("Y-m-d H:i:s");
        $newValues['status'] = User::ACTIVE;
        UserStorage::update($_SESSION['user'], $newValues);
        setcookie(session_name(), session_id(), time()+$lifetime, '/');
    }

    public static function destroySession()
    {
        $newValues['last_active'] = null;
        $newValues['status'] = User::INACTIVE;
        UserStorage::update($_SESSION['user'], $newValues);
        session_unset();
        session_destroy();
        session_write_close();
        setcookie(session_name(), '', 0, '/');
    }
}
