<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 25.4.2019 г.
 * Time: 20:37
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Model\OrderItem;
use ClubSoftware\Mvc\Router;

class OrderItemDropdownStatusButton
{
    private $class;
    private $statusName;
    private $orderItemId;

    public function getButton($status, $orderItemId)
    {
        $this->orderItemId = $orderItemId;
        switch ($status) {
            case OrderItem::STATUS_NEW:
                $this->class = 'btn-danger';
                $this->statusName = 'New';
                break;
            case OrderItem::STATUS_PROGRESS:
                $this->class = 'btn-primary';
                $this->statusName ='Progress';
                break;
            case OrderItem::STATUS_READY:
                $this->class = 'btn-success';
                $this->statusName = 'Ready';
                break;
            case OrderItem::STATUS_SERVED:
                $this->class = 'btn-warning';
                $this->statusName = 'Served';
                break;
            case OrderItem::STATUS_PAYED:
                $this->class = 'btn-secondary';
                $this->statusName = 'Payed';
                break;
        }

        return $this->getHtmlButton();
    }

    private function getHtmlButton()
    {
        $orderId = $_GET['orderId'];
        $dropdownButton = '<div class="dropdown dropleft">';
        $dropdownButton .= '<button class="btn '.$this->class.' w-100 dropdown-toggle" type="button" data-toggle="dropdown">';
        $dropdownButton .= $this->statusName.'</button >';
        $dropdownButton .= '<div class="dropdown-menu">';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orderItems/status', ['orderId' => $orderId, 'orderItemId' => $this->orderItemId, 'statusId' => OrderItem::STATUS_NEW]).'">New</a>';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orderItems/status', ['orderId' => $orderId, 'orderItemId' => $this->orderItemId, 'statusId' => OrderItem::STATUS_PROGRESS]).'">Progress</a>';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orderItems/status', ['orderId' => $orderId, 'orderItemId' => $this->orderItemId, 'statusId' => OrderItem::STATUS_READY]).'">Ready</a>';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orderItems/status', ['orderId' => $orderId, 'orderItemId' => $this->orderItemId, 'statusId' => OrderItem::STATUS_SERVED]).'">Served</a>';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orderItems/status', ['orderId' => $orderId, 'orderItemId' => $this->orderItemId, 'statusId' => OrderItem::STATUS_PAYED]).'">Payed</a>';
        $dropdownButton .= '</div >';
        $dropdownButton .= '</div>';

        return $dropdownButton;
    }
}
