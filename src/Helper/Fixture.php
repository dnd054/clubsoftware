<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 23.12.2018 г.
 * Time: 17:48
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Model\MenuItem;
use ClubSoftware\Storage\MenuItemStorage;
use ClubSoftware\Storage\TableStorage;
use ClubSoftware\Model\Table;

class Fixture
{
    public static function getPath()
    {
        $args = array_slice($_SERVER['argv'], 1);
        $parts = [];
        $params = [];

        foreach ($args as $arg) {
            if (strpos($arg, '--') != 0) {
                continue;
            }
            $arg = str_replace('--', '', $arg);
            $parts = explode('=', $arg);
            $params[$parts[0]] = $parts[1];
        }

        $path = $params[$parts[0]];

        return $path;
    }

    public static function addToStorage()
    {
        $path = self::getPath();

        if (!is_dir($path)) {
            throw new \InvalidArgumentException('Path does not exist!');
        }

        $allFiles = scandir($path);
        $files = array_diff($allFiles, array ('.', '..'));

        if (!isset($files)) {
            throw new \InvalidArgumentException('No Files in Path!');
        }

        foreach ($files as $file) {
            $json_data = json_decode(file_get_contents($path . $file), true);
            $file = explode('.', $file);
            $file = $file[0];

            if ($file != 'Table' && $file != 'MenuItem') {
                throw new \InvalidArgumentException('Wrong filename!');
            } elseif ($file == 'Table') {
                foreach ($json_data as $key => $value) {
                    $class = Table::class;
                    $table = ItemFactory::build($class, $value);
                    TableStorage::add($table);
                }
            } else {
                foreach ($json_data as $key => $value) {
                    $class = MenuItem::class;
                    $menuItem = ItemFactory::build($class, $value);
                    MenuItemStorage::add($menuItem);
                }
            }
        }

        return true;
    }
}