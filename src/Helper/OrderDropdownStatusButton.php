<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 25.4.2019 г.
 * Time: 20:37
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Model\Order;
use ClubSoftware\Mvc\Router;

class OrderDropdownStatusButton
{
    private $class;
    private $statusName;
    private $orderId;

    public function getButton($status, $orderId)
    {
        $this->orderId = $orderId;
        switch ($status) {
            case Order::STATUS_NEW:
                $this->class = 'btn-danger';
                $this->statusName = 'New';
                break;
            case Order::STATUS_PROGRESS:
                $this->class = 'btn-primary';
                $this->statusName ='Progress';
                break;
            case Order::STATUS_FINISHED:
                $this->class = 'btn-success';
                $this->statusName = 'Finished';
                break;
        }

        return $this->getHtmlButton();
    }

    private function getHtmlButton()
    {
        $dropdownButton = '<div class="dropdown dropleft">';
        $dropdownButton .= '<button class="btn '.$this->class.' w-100 dropdown-toggle" type="button" data-toggle="dropdown">';
        $dropdownButton .= $this->statusName.'</button >';
        $dropdownButton .= '<div class="dropdown-menu">';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orders/status', ['orderId' => $this->orderId, 'statusId' => Order::STATUS_NEW]).'">New</a>';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orders/status', ['orderId' => $this->orderId, 'statusId' => Order::STATUS_PROGRESS]).'">Progress</a>';
        $dropdownButton .= '<a class="dropdown-item" href="'.Router::getUrl('/orders/status', ['orderId' => $this->orderId, 'statusId' => Order::STATUS_FINISHED]).'">Finished</a>';
        $dropdownButton .= '</div >';
        $dropdownButton .= '</div>';

        return $dropdownButton;
    }
}
