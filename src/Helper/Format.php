<?php
namespace ClubSoftware\Helper;

class Format
{
    public static function toTable(array $data)
    {
        if (empty($data)) {
            throw new \InvalidArgumentException('No Objects Found!');
        }

        $firstItem = current($data);
        $countColumns = count($firstItem->getOutputFields());

        echo PHP_EOL;

        // Drawing Columns
        $underscores = str_repeat(str_repeat('_', 11), $countColumns);
        echo $underscores, PHP_EOL;

        foreach ($firstItem->getOutputFields() as $field => $value) {
            $field = strtoupper(str_replace("\n", "", $field));
            printf("%-10s|", $field);
        }
        echo PHP_EOL;

        echo $underscores, PHP_EOL;

        // Drawing Data Cells
        foreach ($data as $item) {
            foreach ($item->getOutputFields() as $value) {
                printf("%-10s|", $value($item));
            }
            echo PHP_EOL;
        }

        echo $underscores, PHP_EOL;

        return true;
    }
}