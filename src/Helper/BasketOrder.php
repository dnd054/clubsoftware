<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.3.2019 г.
 * Time: 10:42
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Storage\MenuItemStorage;

class BasketOrder
{
    const SESSION_KEY = 'orderItemsMap';

    public static function add($menuItemId, $quantity)
    {
        $_SESSION[self::SESSION_KEY][$menuItemId] = 0;
        $_SESSION[self::SESSION_KEY][$menuItemId] += $quantity;
    }

    public static function remove($productId)
    {
        foreach ($_SESSION[self::SESSION_KEY] as $key => $value) {
            if ($key == $productId) {
                unset($_SESSION[self::SESSION_KEY][$key]);
            }
        }
    }

    public static function clearBasket()
    {
        unset($_SESSION[self::SESSION_KEY]);
    }

    public static function formatOrderItems($itemsMap)
    {
        $basketItems = [];

        foreach ($itemsMap as $key => $value) {
            $menuItem = MenuItemStorage::get($key);
            $basketItem['id'] = $key;
            $basketItem['name'] = $menuItem->getName();
            $basketItem['price'] = $menuItem->getPrice();
            $basketItem['description'] = $menuItem->getDescription();
            $basketItem['quantity'] = $value;
            $basketItems[] = $basketItem;
        }

        return $basketItems;
    }
}
