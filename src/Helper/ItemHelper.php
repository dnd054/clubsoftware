<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 16.1.2019 г.
 * Time: 23:07
 */

namespace ClubSoftware\Helper;

class ItemHelper
{
    public static function getSetterName($field)
    {
        $field = self::underscoreToPascal($field);
        $field = 'set' . $field;
        return $field;
    }

    public static function getGetterName($field)
    {
        $field = self::underscoreToPascal($field);
        $field = 'get' . $field;
        return $field;
    }

    public static function underscoreToPascal($underscore)
    {
        $pascal = explode('_', $underscore);
        foreach ($pascal as $key => $word) {
            $pascal[$key] = ucfirst($word);
        }
        $pascal = implode('', $pascal);

        return $pascal;
    }
}