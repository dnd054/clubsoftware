<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 31.3.2019 г.
 * Time: 16:37
 */

namespace ClubSoftware\Helper;

use ClubSoftware\Model\Category;
use ClubSoftware\Storage\CategoryStorage;

class CategoriesHelper
{
    private static $all;

    const PATH_SEPARATOR = '/';

    public static function all()
    {
        if (!isset(self::$all)) {
            $result = CategoryStorage::all();
            self::$all = [];

            foreach ($result as $category) {
                self::$all[$category->getId()] = $category;
            }
        }

        return self::$all;
    }

    public static function getPath(Category $category)
    {
        $categories = self::getCategories($category->getPath());
        $names = array_map(function ($value) {
            return $value->getName();
        }, $categories);

        return implode(self::PATH_SEPARATOR, $names);
    }

    public static function getCategories($path)
    {
        $categories = [];
        $ids = explode(self::PATH_SEPARATOR, $path);

        foreach ($ids as $id) {
            $all = self::all();

            if (!empty($all[$id])) {
                $categories[] = $all[$id];
            }
        }

        return $categories;
    }
}