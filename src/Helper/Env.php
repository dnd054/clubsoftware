<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 24.1.2019 г.
 * Time: 15:30
 */

namespace ClubSoftware\Helper;


use Dotenv\Dotenv;

class Env
{
    private static $dotenv;

    private static function loadEnv()
    {
        if (isset(self::$dotenv)) {
            return;
        }

        self::$dotenv = Dotenv::create(__DIR__ . '/../../');
        self::$dotenv->load();
    }

    public static function get($key, $default = null)
    {
        self::loadEnv();
        return isset($_ENV[$key]) ? $_ENV[$key] : $default;
    }
}