<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 14.3.2019 г.
 * Time: 20:53
 */

namespace ClubSoftware\Helper;

class FormatDateTime
{
    public static function toTable($mysqlTime)
    {
        $strTime = strtotime($mysqlTime);
        $tableTime = date('d-M-Y H:i:s', $strTime);

        return $tableTime;
    }
}