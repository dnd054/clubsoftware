<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 28.3.2019 г.
 * Time: 8:38
 */

namespace ClubSoftware\Helper;

class CategoryList
{
    protected $categoryId;
    protected $categories = [];

    public function __construct($categoryId = '')
    {
        $this->categoryId = $categoryId;
        $this->categories = CategoriesHelper::all();
    }

    public function __toString()
    {
        return $this->buildList();
    }

    public function hasChildren($categories, $id)
    {
        foreach ($categories as $category) {
            if ($category['parent_id'] == $id) {
                return true;
            }
        }
        return false;
    }

    public function makeTree($categories, $parent = 0)
    {
        $result = [];
        foreach ($categories as $arrayCategory) {
            if ($arrayCategory['parent_id'] == $parent) {
                $result[$arrayCategory['id']] = $arrayCategory;
                if ($this->hasChildren($categories, $arrayCategory['id'])) {
                    $arrayCategory['children'] = $this->makeTree($categories, $arrayCategory['id']);
                    $result[$arrayCategory['id']]['children'] = $arrayCategory['children'];
                }
            }
        }

        return $result;
    }

    public function getTree($categories, $padding = '')
    {
        $string = '';

        foreach ($categories as $key => $value) {
            $isSelected = '';
            if ($this->categoryId == $value['id']) {
                $isSelected = 'selected';
            }
            $string .= '<option value="'.$value['id'].'" '.$isSelected.'>'. $padding . $value['name'] .'</option>';
            if (!empty($value['children'])) {
                $string .= $this->getTree($value['children'], $padding . '&nbsp;&nbsp;&nbsp;');
            }
        }

        return $string;
    }

    public function categoryToArray($categories)
    {
        $arrayCategories = [];
        $arrayCategory = [];

        foreach ($categories as $category) {
            foreach ($category->getOutputFields() as $field => $stringRenderer) {
                $arrayCategory[$field] = $stringRenderer($category);
            }
            $arrayCategories[] = $arrayCategory;
        }
        return $arrayCategories;
    }

    public function buildList()
    {
        $categories = $this->categories;
        $arrayCategories = $this->categoryToArray($categories);
        $categoryTree = $this->makeTree($arrayCategories);
        $categoryList = $this->getTree($categoryTree);

        return $categoryList;
    }
}