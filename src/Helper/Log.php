<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 20.1.2019 г.
 * Time: 10:01
 */

namespace ClubSoftware\Helper;


class Log
{
    public static function log($message)
    {
        $log = sprintf('[%s] %s', date('Y-m-d H:i:s'), $message . PHP_EOL);
        static::writeLog($log);
    }

    private static function writeLog($log)
    {
        $path = self::realPath(). '/application.log';
        file_put_contents($path, $log, FILE_APPEND);
    }

    private static function realPath()
    {
        $path = __DIR__. '/../../logs';
        return realpath($path);
    }
}