<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 17.12.2018 г.
 * Time: 18:16
 */

namespace ClubSoftware\Storage;

use ClubSoftware\Model\OrderItem;
use ClubSoftware\Model\Order;

class OrderStorage extends ItemStorage
{
    protected static $sqlTable = 'orders';
    protected static $class = Order::class;

    public static function addOrderItem(Order $order)
    {
        do {
            $itemKey = readline("Select Menu Item to Order: ");
            $choice = MenuItemStorage::get($itemKey);
            $orderItem = new OrderItem($choice);
            $firstRun = true;

            $orderId = $order->getId();
            $orderItem->setOrderId($orderId);
            $orderItem->setMenuItemId($choice->getId());

            foreach ($choice as $name => $value) {
                if ($firstRun != true) {
                    $setter = 'set' . ucfirst($name);
                    if (method_exists($orderItem, $setter)) {
                        $orderItem->$setter($value);
                    }
                }
                $firstRun = false;
            }

            $orderItems[] = $orderItem;

            $continue = readline("Select Another Item? (y/n): ");

        } while ($continue != 'n');

        return $orderItems;
    }
}