<?php
namespace ClubSoftware\Storage;

use ClubSoftware\Model\Category;

class CategoryStorage extends ItemStorage
{
    protected static $key = 1;
    protected static $collection = [];
    protected static $sqlTable = 'categories';
    protected static $class = Category::class;
}
