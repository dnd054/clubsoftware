<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 12.4.2019 г.
 * Time: 16:59
 */

namespace ClubSoftware\Storage;

use ClubSoftware\Model\Source;

class SourceStorage extends ItemStorage
{
    protected static $key = 1;
    protected static $collection = [];
    protected static $sqlTable = 'menu_item_sources';
    protected static $class = Source::class;
}