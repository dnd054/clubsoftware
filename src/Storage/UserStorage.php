<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 16:03
 */

namespace ClubSoftware\Storage;

use ClubSoftware\Model\User;

class UserStorage extends ItemStorage
{
    protected static $sqlTable = 'users';
    protected static $class = User::class;
}