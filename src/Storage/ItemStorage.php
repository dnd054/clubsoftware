<?php

namespace ClubSoftware\Storage;

use ClubSoftware\Db\Connection;
use ClubSoftware\Helper\ItemFactory;
use ClubSoftware\Helper\ItemHelper;
use ClubSoftware\Db\Query;

abstract class ItemStorage
{
    protected static $class;
    protected static $sqlTable;

    public static function add($item)
    {
        $columns = Connection::getTableFields(static::$sqlTable);
        $params = [];
        $placeHolder = [];

        foreach ($columns as $column) {
            $getter = ItemHelper::getGetterName($column);
            $params[] = $item->$getter();
            $placeHolder[] = '?';
        }
        $columns = implode(', ', $columns);
        $placeHolder = implode(', ', $placeHolder);

        $sql = sprintf("INSERT INTO %s (%s) VALUES (%s);", static::$sqlTable, $columns, $placeHolder);
        $result = Connection::query($sql, $params);
        $item->setId($result);

        return $result;
    }

    public static function get($key)
    {
        $q = (new Query())->from(static::$sqlTable)->where('id', $key);

        return current(static::all($q));
    }

    public static function getLastId()
    {
        $sql = sprintf("SELECT id FROM %s ORDER BY id DESC LIMIT 1;", static::$sqlTable);
        $query = Connection::query($sql);
        if (!empty($query)) {
            return $query[0]['id'];
        } else {
            return 0;
        }
    }

    public static function all(Query $query = null)
    {
        if (!isset($query)) {
            $query = new Query();
            $query->from(static::$sqlTable);
        }
        if (!$query->getFromTable()) {
            $query->from(static::$sqlTable);
        }
        $rawQuery = self::rawQuery($query);
        $result = self::sendToFactory($rawQuery);

        return $result;
    }

    public static function rawQuery($query)
    {
        list($sql, $params) = $query->build();
        $rawQuery = Connection::query($sql, $params);

        return $rawQuery;
    }

    public static function delete($key)
    {
        $sql = sprintf("DELETE FROM %s WHERE id = %d;", static::$sqlTable, $key);
        $result = Connection::query($sql);

        return $result;
    }

    public static function update($model, $newValues)
    {
        $columns = [];
        $params = [];

        foreach ($newValues as $key => $value) {
            $setter = ItemHelper::getSetterName($key);
            if (method_exists($model, $setter)) {
                $model->$setter($value);
                $columns[] = $key . "=?";
                $params[] = $value;
            }
        }

        $params[] = $model->getId();
        $columns = implode(', ', $columns);
        $sql = sprintf("UPDATE %s SET %s WHERE id = ?;", static::$sqlTable, $columns);
        $result = Connection::query($sql, $params);

        return $result;
    }

    public static function sendToFactory($rawQuery)
    {
        $result = [];

        foreach ($rawQuery as $key => $value) {
            $item = ItemFactory::build(static::$class, $value);
            $result[] = $item;
        }

        return $result;
    }
}