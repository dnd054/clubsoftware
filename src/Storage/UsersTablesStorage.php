<?php

namespace ClubSoftware\Storage;

use ClubSoftware\Db\Connection;
use ClubSoftware\Db\Query;

class UsersTablesStorage extends ItemStorage
{
    protected static $storage;
    protected static $class;
    protected static $sqlTable;

    public static function getFreeTables()
    {
        $freeTables = [];

        $sql = sprintf("SELECT tables.id
                FROM tables 
                LEFT JOIN %s 
                ON %s.id_table = tables.id
                WHERE %s.id_table IS NULL;", static::$sqlTable, static::$sqlTable, static::$sqlTable)
            ;

        $tables = Connection::query($sql);

        if (!empty($tables)) {
            foreach ($tables as $table) {
                $freeTables[] = TableStorage::get($table['id']);
            }
        }

        return $freeTables;
    }

    public static function assignTable($userId, $tableId)
    {
        $userTable = new static::$class;
        $userTable->setIdUser($userId);
        $userTable->setIdTable($tableId);
        static::$storage::add($userTable);

        return;
    }

    public static function unAssignTables($userId)
    {
        $sql = sprintf("DELETE FROM %s WHERE id_user = %d;", static::$sqlTable, $userId);
        $result = Connection::query($sql);

        return $result;
    }

    public static function getUsersTables($userId)
    {
        $query = (new Query())->where('id_user', $userId);
        $usersTables = static::$storage::all($query);

        return $usersTables;
    }

    public static function getCheckboxList($tables, $assigned)
    {
        $checkboxList = [];
        if (!empty($tables)) {
            $checked = ($assigned) ? 'checked' : '';

            foreach ($tables as $table) {
                $checkbox = '<li class="list-group-item">';
                $checkbox .= $table->getName();
                $checkbox .= '<label class="checkbox">';
                $checkbox .= '<input type="checkbox" name="tableIds[]" value="'.$table->getId().'" '.$checked.'>';
                $checkbox .= '<span class="success"></span>';
                $checkbox .= '</label>';
                $checkbox .= '</li>';
                $checkboxList[] = $checkbox;
            }
        }

        return $checkboxList;
    }
}
