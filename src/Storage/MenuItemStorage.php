<?php
namespace ClubSoftware\Storage;

use ClubSoftware\Model\MenuItem;

class MenuItemStorage extends ItemStorage
{
    protected static $key = 1;
    protected static $collection = [];
    protected static $sqlTable = 'menu_items';
    protected static $class = MenuItem::class;
}