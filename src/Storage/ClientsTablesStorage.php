<?php

namespace ClubSoftware\Storage;

use ClubSoftware\Model\ClientsTables;

class ClientsTablesStorage extends UsersTablesStorage
{
    protected static $key = 1;
    protected static $collection = [];
    protected static $sqlTable = 'clients_tables';
    protected static $class = ClientsTables::class;
    protected static $storage = self::class;
}
