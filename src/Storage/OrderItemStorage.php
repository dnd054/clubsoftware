<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 16.1.2019 г.
 * Time: 22:34
 */

namespace ClubSoftware\Storage;

use ClubSoftware\Model\OrderItem;

class OrderItemStorage extends ItemStorage
{
    protected static $sqlTable = 'order_items';
    protected static $class = OrderItem::class;
}