<?php

namespace ClubSoftware\Storage;

use ClubSoftware\Model\PersonelTables;

class PersonelTablesStorage extends UsersTablesStorage
{
    protected static $key = 1;
    protected static $collection = [];
    protected static $sqlTable = 'personel_tables';
    protected static $class = PersonelTables::class;
    protected static $storage = self::class;
}