<?php
namespace ClubSoftware\Storage;

use ClubSoftware\Model\Table;

class TableStorage extends ItemStorage
{
    protected static $sqlTable = 'tables';
    protected static $class = Table::class;
}