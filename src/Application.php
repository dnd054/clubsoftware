<?php

namespace ClubSoftware;

    use ClubSoftware\Db\JoinDescriptor;
    use ClubSoftware\Helper\Format;
    use ClubSoftware\Db\Query;
    use ClubSoftware\Model\OrderManagement;
    use ClubSoftware\Model\InputFields;
    use ClubSoftware\Model\MenuItem;
    use ClubSoftware\Model\Order;
    use ClubSoftware\Model\Table;
    use ClubSoftware\Storage\MenuItemStorage;
    use ClubSoftware\Storage\OrderItemStorage;
    use ClubSoftware\Storage\OrderStorage;
    use ClubSoftware\Storage\TableStorage;

class Application
{
    public static function run()
    {
        $prompt = "ClubSoftware Application Interface 
          Choose Class for Object: 
          1. New Order 
          2. List Order
          3. List All Orders
          4. Update Order
          5. Remove Order
          6. Create New Table
          7. List Tables
          8. Remove Table
          9. Add Menu Item
          10. List Menu Items
          11. Update Menu Item
          12. Remove Menu Item
          13. New MySQL Query
          0. Exit
          ";

        do {
            echo $prompt;
            $choice = readline();
            switch ($choice) {
                case 1:
                    self::makeOrder();
                    break;
                case 2:
                    self::listOrder();
                    break;
                case 3:
                    self::listAllOrders();
                    break;
                case 4:
                    self::updateOrder();
                    break;
                case 5:
                    self::deleteOrder();
                    break;
                case 6:
                    self::createTable();
                    break;
                case 7:
                    self::listTables();
                    break;
                case 8:
                    self::deleteTable();
                    break;
                case 9:
                    self::addMenuItem();
                    break;
                case 10:
                    self::listMenuItems();
                    break;
                case 11:
                    self::updateMenuItem();
                    break;
                case 12:
                    self::deleteMenuItem();
                    break;
                case 13:
                    self::testQuery();
                    break;
            }
        } while ($choice != 0);
    }

    public static function makeOrder()
    {
        $tableID = readline("Select table: ");
        $table = TableStorage::get($tableID);
        self::listMenuItems();
        $order = new Order();
        $order->setTable($table);
        $order->setStatus(Order::STATUS_NEW);
        OrderStorage::add($order);
        $orderItems = OrderStorage::addOrderItem($order);
        if (OrderManagement::sendToSource($orderItems)) {
            $order->setStatus(Order::STATUS_PROGRESS);
        } else {
            OrderStorage::delete($order->getId());
        }
    }

    public static function listOrder()
    {
        $orderId = readline("Select Order: ");
        $q = (new Query())->from('order_items')->where('order_id', $orderId);
        $orderItems = OrderItemStorage::all($q);
        $order = OrderStorage::get($orderId);
        $order->setOrderItems($orderItems);
        $table = $order->getTableId();
        $status = $order->getStatus();

        echo "OrderID $orderId |TableID $table |Status -> $status";

        Format::toTable($orderItems);
    }


    public static function listAllOrders()
    {
        $q = (new Query())->from('orders');
        $orders = OrderStorage::all($q);

        foreach ($orders as $order) {
            $orderId = $order->getId();
            $q = (new Query())->from('order_items')->where('order_id', $orderId);
            $orderItems = OrderItemStorage::all($q);
            $order->setOrderItems($orderItems);
            $table = $order->getTableId();
            $status = $order->getStatus();

            echo "OrderID $orderId |TableID $table |Status -> $status";

            Format::toTable($orderItems);
        }
    }

    public static function updateOrder()
    {
        $orderId = readline("Select Order: ");
        $q = (new Query())->from('order_items')->where('order_id', $orderId);
        $orderItems = OrderItemStorage::all($q);
        $order = OrderStorage::get($orderId);
        $order->setOrderItems($orderItems);
        $newValues = [];
        // Update order table_id, status
        if (readline("Do you want to update order data? y/n: ") == 'y') {
            $newValues = self::readFieldValues($order);
            OrderStorage::update($order, $newValues);
        }
        // Update existing orderItem (source, status)
        if (readline("Do you want to update order item? y/n: ") == 'y') {
            $id = readline("Select OrderItem: ");
            $orderItem = current(array_filter($orderItems, function ($value) use ($id) {
                return $value->getId() == $id;
            }));
            $newValues = self::readFieldValues($orderItem);
            OrderItemStorage::update($orderItem, $newValues);
        }
        // Add new orderItem
        if (readline("Do you want to add another item? y/n: ") == 'y') {
            $orderItems = OrderStorage::addOrderItem($order);
            foreach ($orderItems as $item) {
                OrderItemStorage::add($item);
            }
            $orderItems = array_merge($order->getOrderItems(), $orderItems);
            $order->setOrderItems($orderItems);
            var_dump($order);
        }
        // Delete
        if (readline("Do you want to delete an item? y/n: ") == 'y') {
            $orderItemId = readline("Select Item: ");
            $result = OrderItemStorage::delete($orderItemId);

            if ($result) {
                echo "$result Order Item Deleted!", PHP_EOL;
            } else {
                echo "Order Item Not Found!", PHP_EOL;
            }
            $orderItems = array_filter($order->getOrderItems(), function ($value) use ($orderItemId) {
                return $value->getId() != $orderItemId;
            });
            $order->setOrderItems($orderItems);
        }
    }

    public static function deleteOrder()
    {
        $orderID = readline("Select Order to be Removed: ");
        $result = OrderStorage::delete($orderID);

        if ($result) {
            echo "$result Order Deleted!", PHP_EOL;
        } else {
            echo "Order Not Found!", PHP_EOL;
        }

        return true;
    }

    public static function createTable()
    {
        $tableName = readline("Enter Table Name: ");
        $table = new Table();
        $table->setName($tableName);
        TableStorage::add($table);
    }

    public static function listTables()
    {
        $q = (new Query())->from('tables');
        Format::toTable(TableStorage::all($q));
    }

    public static function deleteTable()
    {
        $itemID = readline("Select Table to be Removed: ");
        $result = TableStorage::delete($itemID);
        if ($result) {
            echo $result . " Table Deleted!", PHP_EOL;
        } else {
            echo "Table Not Found!", PHP_EOL;
        }

        return true;
    }

    public static function addMenuItem()
    {
        $itemName = readline("Set Item Name: ");
        $itemPrice = readline("Set Item Price: ");
        $itemSource = readline("Set Item Source (1-Bar|2-Kitchen): ");
        $menuItem = new MenuItem();
        $menuItem->setName($itemName);
        $menuItem->setPrice($itemPrice);
        $menuItem->setSourceId($itemSource);
        MenuItemStorage::add($menuItem);
    }

    public static function updateMenuItem()
    {
        $key = readline("Enter ID of the Item to be Updated: ");
        $model = MenuItemStorage::get($key);
        $newValues = self::readFieldValues($model);
        MenuItemStorage::update($model, $newValues);
    }

    public static function deleteMenuItem()
    {
        $itemID = readline("Select Menu Item to be Removed: ");
        $result = MenuItemStorage::delete($itemID);

        if ($result) {
            echo "$result Item Deleted!", PHP_EOL;
        } else {
            echo "Item Not Found!", PHP_EOL;
        }

        return true;
    }

    public static function listMenuItems()
    {
        $q = (new Query())->from('menu_items');
        Format::toTable(MenuItemStorage::all($q));
    }

    public static function readFieldValues(InputFields $fields)
    {
        $input = [];

        foreach ($fields->getInputFields() as $name) {
            $userInput = readline("Enter new " . strtoupper($name) . ": ");
            if ($userInput) {
                $input[$name] = $userInput;
            }
        }

        return $input;
    }

    public static function testQuery()
    {
        $query = (new Query())
            ->select('order_items.id')
            ->select('menu_items.name')
            ->select('menu_items.source_id')
            ->select('order_items.status')
            ->from('menu_items')
            ->join(JoinDescriptor::LEFT_JOIN, 'order_items', 'menu_items.id = order_items.menu_item_id')
            ->join(JoinDescriptor::LEFT_JOIN, 'menu_item_sources', 'menu_items.source_id = menu_item_sources.id')
            ->where('order_items.order_id', 2)
        ;

        var_dump(OrderItemStorage::all($query));
    }
}