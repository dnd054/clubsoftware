<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 22.01.19
 * Time: 19:00
 */

namespace ClubSoftware\Db;

/***
 * Class Query
 * @package ClubSoftware\Helper
 * SELECT
 *  name, id,
 * FROM table a
 * LEFT JOIN table1 b ON b.key=a.value
 * WHERE
 * GROUP
 * ORDER
 * LIMIT
 */
class Query
{
    private $selectFields = [];
    private $fromTable;
    private $join = [];
    private $where = [];
    private $orderBy = [];
    private $limit;
    private $groupBy;
    private $offset = 0;

    public function setSelectFields($selectFields)
    {
        $this->selectFields = $selectFields;
    }

    public function getSelectFields(): array
    {
        return $this->selectFields;
    }

    public function select($field)
    {
        $this->selectFields[] = $field;
        return $this;
    }

    public function getFromTable()
    {
        return $this->fromTable;
    }

    public function from($table)
    {
        $this->fromTable = $table;
        return $this;
    }

    public function getJoin(): array
    {
        return $this->join;
    }

    public function join($type, $table, $condition)
    {
        $joinDescriptor = new JoinDescriptor($type, $table, $condition);
        $this->join[] = $joinDescriptor;

        return $this;
    }

    public function setJoin(array $join)
    {
        $this->join = $join;
        return $this;
    }

    public function getWhere(): array
    {
        return $this->where;
    }

    public function where($field, $value)
    {
        $this->where[] = [
            $field,
            $value
        ];
        return $this;
    }

    public function getOrderBy(): array
    {
        return $this->orderBy;
    }

    public function orderBy($orderBy)
    {
        $this->orderBy[] = $orderBy;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getGroupBy()
    {
        return $this->groupBy;
    }

    public function groupBy($groupBy)
    {
        self::setGroupBy($groupBy);
        return $this;
    }

    public function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;
        return $this;
    }

    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function build()
    {
        $sql = sprintf('SELECT %s FROM %s', self::makeSelect($this->selectFields), $this->fromTable);

        $sql .= self::makeJoin($this->join);
        $sql .= self::makeWhere($this->where);
        $sql .= self::makeOrderBy($this->orderBy);
        $sql .= self::makeLimit($this->offset, $this->limit);
        $sql .= self::makeGroupBy($this->groupBy);
        $params = self::getParams($this->where);

        return [
            $sql,
            $params
        ];
    }

    private static function makeSelect($selectFields)
    {
        if (empty($selectFields)) {
            $selectFields = ['*'];
        }
        return implode(', ', $selectFields);
    }

    private static function makeJoin($joinDescriptors)
    {
        return ' ' . implode(' ', $joinDescriptors);
    }

    private static function makeWhere($statement)
    {
        if (!empty($statement)) {
            $where = [];
            foreach ($statement as $condition) {
                $where[] = $condition[0] . ' = ?';
            }

            return ' WHERE ' . implode(' AND ', $where);
        }
    }

    private static function getParams($statement)
    {
        $params = [];
        foreach ($statement as $condition) {
            $params[] = $condition[1];
        }

        return $params;
    }

    private static function makeOrderBy($statement)
    {
        if (!empty($statement)) {
            $orderBy = [];
            foreach ($statement as $condition) {
                if (isset($condition)) {
                    $orderBy[] = ' ORDER BY ' . $condition;
                }
            }

            return implode(', ', $orderBy);
        }
    }

    private static function makeLimit($offset, $limit)
    {
        if (empty($limit)) {
            return '';
        }

        if (empty($offset)) {
            $offset = 0;
        }

        $statement[] = $offset;
        $statement[] = $limit;

        $statement = implode(', ', $statement);
        return ' LIMIT ' . $statement;
    }

    private static function makeGroupBy($statement)
    {
        if (!empty($statement)) {
            $groupBy = ' GROUP BY ' . $statement;

            return $groupBy;
        }
    }
}
