<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 30.1.2019 г.
 * Time: 13:07
 */

namespace ClubSoftware\Db;

class JoinDescriptor
{
    const LEFT_JOIN = 'LEFT JOIN';
    const RIGHT_JOIN = 'RIGHT JOIN';
    const INNER_JOIN = 'INNER JOIN';
    const OUTER_JOIN = 'OUTER JOIN';

    private $type;
    private $table;
    private $condition;

    public function __construct($type, $table, $condition)
    {
        $this->type = $type;
        $this->table = $table;
        $this->condition = $condition;
    }

    public function __toString()
    {
        $join = sprintf("%s %s ON %s", $this->type, $this->table, $this->condition);

        return $join;
    }
}