<?php
namespace ClubSoftware\Db;

use ClubSoftware\Helper\Env;
use ClubSoftware\Helper\Log;
use PDO;

class Connection
{
    private static $pdo;

    private static function getConnection()
    {
        if (!isset(self::$pdo)) {
            $dsn = 'mysql:host=' . Env::get('DB_SERVER') . ';dbname=' . Env::get('DB_NAME') . ';charset=utf8';
            $pdo = new PDO($dsn, Env::get('DB_USER'), Env::get('DB_PASSWORD'));
            // Set the PDO error mode to exception
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$pdo = $pdo;
        }
        return self::$pdo;
    }

    public static function query($sql, $params = [])
    {
        try {
            $connection = self::getConnection();

            $statement = $connection->prepare($sql);
            Log::log($sql);
            $statement->execute($params);
            $sqlArray = explode(' ', $sql);
            switch ($sqlArray[0]) {
                case 'INSERT':
                    return $connection->lastInsertId();
                    break;
                case 'SELECT':
                    return $statement->fetchAll(PDO::FETCH_ASSOC);
                    break;
                case 'UPDATE':
                case 'DELETE':
                    return $statement->rowCount();
                    break;
            }
        } catch (\Throwable $t) {
            var_dump($sql, $params);
            throw $t;
        }
    }

    public static function getTableFields($table)
    {
        $connection = self::getConnection();
        $tableFields = $connection->query("SHOW COLUMNS FROM $table")->fetchAll(PDO::FETCH_COLUMN);

        return $tableFields;
    }
}

