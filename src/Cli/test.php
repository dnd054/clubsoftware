<?php
namespace ClubSoftware\Cli;

use ClubSoftware\Helper\CategoriesHelper;

$categories = CategoriesHelper::all();
var_dump($categories);
die();

function hasChildren($categories, $id)
{
    foreach ($categories as $category) {
        if ($category['parent_id'] == $id) {
            return true;
        }
    }
    return false;
}
function makeTree($categories, $parent = 0)
{
    $result = [];
    foreach ($categories as $category) {
        if ($category['parent_id'] == $parent) {
            $result[$category['id']] = $category;
            if (hasChildren($categories, $category['id'])) {
                $category['children'] = makeTree($categories, $category['id']);
                $result[$category['id']]['children'] = $category['children'];
            }
        }
    }

    return $result;
}

function getTree($categories, $padding = '')
{
    $string = '';

    foreach ($categories as $key => $value) {
        $string .= $padding . $value['id'] . PHP_EOL;
        if (!empty($value['children'])) {
            $string .= getTree($value['children'], $padding . ' ');
        }
    }

    return $string;
}

function buildList()
{
    $categories = $this->categories;

    $categoryList = '<option class="bg-light" value="" selected>Select Category</option>';
    foreach ($categories as $category) {
        $path = $category->getPath();
        $pieces = explode(CategoriesHelper::PATH_SEPARATOR, $path);
        $pieces = array_filter($pieces);
        $spaces = count($pieces);
        $padding = '';
        if ($spaces) {
            $padding = str_repeat('&nbsp;&nbsp;&nbsp;', $spaces);
        }
        $categoryList .= '<option value="'.$category->getId() .'">'. $padding . $category->getTitle() .'</option>';
    }

    return $categoryList;
}

function retChildren($categories, $id)
{
    $children = [];
    foreach ($categories as $category) {
        if ($category->getParentId() == $id) {
            $children[] = $category;
        }
    }

    return $children;
}

function getCategoryTree($categories, $padding = '', $parent = '')
{
    $categoryTree = '';
    foreach ($categories as $category) {
        if ($category->getParentId() == $parent) {
            if (!empty($this->hasChildren($categories, $category->getId()))) {
                $categoryTree .= '<option value="'.$category->getId() .'">'. $padding . $category->getTitle() .'</option>';
                $childCategories = $this->hasChildren($categories, $category->getId());
                $categoryTree .= $this->getCategoryTree($childCategories, $padding . '&nbsp;&nbsp;&nbsp;', $parent = $category->getId());
            }
            $categoryTree .= '<option value="'.$category->getId() .'">'. $padding . $category->getTitle() .'</option>';
        }
    }

    return $categoryTree;
}
$result = makeTree($categories);
print_r(getTree($result));
