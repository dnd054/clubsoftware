<?php

require_once __DIR__ . '/../../vendor/autoload.php';
$username = @$_SERVER['argv'][1];
$password = @$_SERVER['argv'][2];
$role = @$_SERVER['argv'][3];

$user = new \ClubSoftware\Model\User();
$user->setPassword(password_hash($password, PASSWORD_DEFAULT));
$user->setUsername($username);
$user->setRole($role);
\ClubSoftware\Storage\UserStorage::add($user);
