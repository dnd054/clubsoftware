<?php

namespace ClubSoftware\Mvc;

class Application
{
    public static $config;

    public static function run($config)
    {
        $result = null;
        session_start();

        self::$config = $config;

        try {
            // find out which is the controller
            // https://server.com/<route>
            // https://server.com?route=<route>;
            Router::setRoutes($config['routes']);
            $controllerInstance = Router::route();
            // execute it
            if ($controllerInstance->preAction()) {
                $result = $controllerInstance->doAction();
            }
            // return its result as response
        } catch (\Throwable $t) {
            $result = $t->getMessage();
            $result .= $t->getFile();
            $result .= $t->getLine();
        }
        echo $result;
    }

    public static function stop()
    {
        die();
    }
}