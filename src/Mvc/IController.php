<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:45
 */

namespace ClubSoftware\Mvc;

interface IController
{
    public function preAction();
    public function doAction();
    public function redirect($url);
    public function render($view, $params = []);
}
