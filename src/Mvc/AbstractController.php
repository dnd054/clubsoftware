<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 15:01
 */

namespace ClubSoftware\Mvc;

abstract class AbstractController implements IController
{
    public function preAction()
    {
        return true;
    }

    public function redirect($url)
    {
        header('Location:' . $url);
        Application::stop();
    }

    public function render($view, $params = [])
    {
        return new Template($this->getViewPath() . '/' . $view, $params);
    }

    protected function getViewPath()
    {
        return __DIR__ . '/../App/views';
    }
}
