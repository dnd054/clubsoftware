<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 6.4.2019 г.
 * Time: 20:52
 */

namespace ClubSoftware\Mvc;

class Template
{
    private $view;
    private $params;

    public function __construct($view, $params = [])
    {
        $this->view = $view;
        $this->params = $params;
    }

    public function __toString()
    {
        extract($this->params);
        $path = realpath($this->view . '.php');
        ob_start();
        require $path;
        return ob_get_clean();
    }
}
