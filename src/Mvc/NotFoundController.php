<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 14:32
 */

namespace ClubSoftware\Mvc;

class NotFoundController extends AbstractController
{
    public function doAction()
    {
        return 'Not Found';
    }
}