<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 13:41
 */

namespace ClubSoftware\Mvc;

class Router
{
    const DEFAULT_ROUTE = '*';

    private static $routes;

    /**
     * @param mixed $routes
     */
    public static function setRoutes($routes)
    {
        self::$routes = $routes;
    }

    /**
     * Finds path and its current controller,
     * creates instance of it and returns it
     * to Application.php to doAction
     * @param $routes
     * @return mixed
     */
    public static function route()
    {
        $path = self::analizeUrl();

        $currentController = self::getCurrentControllerClass($path, self::$routes);
        $ctrl = new $currentController();
        return $ctrl;
    }

    private static function analizeUrl()
    {
        $parts = parse_url($_SERVER['REQUEST_URI']);

        return empty($parts['path']) ? '/' : $parts['path'];
    }

    private static function getCurrentControllerClass($path, $routes)
    {
        $default = current(array_filter($routes, function ($v) {
            return $v['route'] === self::DEFAULT_ROUTE;
        }));
        $default = $default ? $default : NotFoundController::class;

        $currentController = null;
        foreach ($routes as $id => $data) {
            list($route, $controllerClass) = array_values($data);
            if ($route === '/') {
                if ($route === $path) {
                    $currentController = $controllerClass;
                    break;
                }
            } elseif (strpos($path, $route) === 0) {
                $currentController = $controllerClass;
                break;
            }
        }

        return $currentController ? $currentController : $default;
    }

    public static function getUrl($routeId, $params = [])
    {
        if (isset(self::$routes[$routeId])) {
            return implode('/?', [self::$routes[$routeId]['route'], http_build_query($params)]);
        }
        throw new \Exception('Route Not Found!'. $routeId);
    }
}
