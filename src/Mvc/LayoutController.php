<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 15:40
 */

namespace ClubSoftware\Mvc;

use ClubSoftware\App\User;
use ClubSoftware\Helper\SessionHandler;

abstract class LayoutController extends AbstractController
{
    protected $layout = 'layouts/login';
    protected $title = 'ClubSoftware';

    public function preAction()
    {
        $controller = str_replace('ClubSoftware\App\Controller\\', '', get_called_class());

        if ($controller != 'LoginController') {
            if (!User::isLogged()) {
                $this->redirect('/login');
            }
            SessionHandler::updateSession();
        }

        return parent::preAction();
    }

    public function render($view, $params = [])
    {
        $params['title'] = $this->title;
        $content = parent::render($view, $params);
        $layoutPath = $this->getViewPath() . '/' . $this->layout;
        $params['content'] = $content;

        return new Template($layoutPath, $params);
    }
}
