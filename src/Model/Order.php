<?php
namespace ClubSoftware\Model;

use ClubSoftware\Storage\TableStorage;

class Order implements InputFields, OutputFields
{
    private $id;
    private $table;
    private $status;
    private $tableId;
    private $orderItems;

    const STATUS_NEW = 1;
    const STATUS_PROGRESS = 2;
    const STATUS_FINISHED = 3;

    public function getTableId()
    {
        return $this->tableId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        if (!isset($this->table)) {
            $this->table = TableStorage::get($this->tableId);
        }
        return $this->table;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table)
    {
        $this->table = $table;
        $this->tableId = $this->table->getId();
    }

    public function setTableId($tableId)
    {
        $this->table = null;
        $this->tableId = $tableId;
    }

    /**
     * @return mixed
     */
    public function getOrderItems()
    {
        return $this->orderItems;
    }

    /**
     * @param mixed $orderItems
     */
    public function setOrderItems($orderItems)
    {
        $this->orderItems = $orderItems;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getInputFields()
    {
        return ['id', 'table_id', 'status'];
    }

    public function getOutputFields()
    {
        return [
            'id' => function (Order $order) {
                return $order->getId();
            },
            'table' => function (Order $order) {
                $table = TableStorage::get($order->getTableId());
                return $table->getName();
            },
            'status' => function (Order $order) {
                if ($order->getStatus() == Order::STATUS_NEW) {
                    return 'New';
                } elseif ($order->getStatus() == Order::STATUS_PROGRESS) {
                    return 'Progress';
                }
                return 'Finished';
            }
        ];
    }
}
