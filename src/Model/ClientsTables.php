<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 10.4.2019 г.
 * Time: 10:53
 */

namespace ClubSoftware\Model;

class ClientsTables
{
    private $id;
    private $idUser;
    private $idTable;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getIdTable()
    {
        return $this->idTable;
    }

    /**
     * @param mixed $idTable
     */
    public function setIdTable($idTable)
    {
        $this->idTable = $idTable;
    }
}
