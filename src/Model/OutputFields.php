<?php
namespace ClubSoftware\Model;

interface OutputFields
{
    public function getOutputFields();
}