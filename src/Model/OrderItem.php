<?php
namespace ClubSoftware\Model;

use ClubSoftware\Storage\MenuItemStorage;
use ClubSoftware\Storage\OrderStorage;
use ClubSoftware\Storage\SourceStorage;

class OrderItem implements InputFields, OutputFields
{

    private $id;
    private $orderId;
    private $menuItem;
    private $menuItemId;
    private $status = self::STATUS_NEW;
    private $order;
    protected static $sqlTable = 'order_items';
    protected static $class = OrderItem::class;
    const STATUS_NEW = 1;
    const STATUS_PROGRESS = 2;
    const STATUS_READY = 3;
    const STATUS_SERVED = 4;
    const STATUS_PAYED = 5;

    /**
     * @return mixed
     */
    public function getOrder()
    {
        if (!isset($this->order)) {
            $this->order = OrderStorage::get($this->orderId);
        }
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getMenuItemId()
    {
        return $this->menuItemId;
    }

    /**
     * @param mixed $menuItemId
     */

    public function setMenuItemId($menuItemId)
    {
        $this->menuItemId = $menuItemId;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return MenuItem
     */
    public function getMenuItem(): MenuItem
    {
        if (!isset($this->menuItem)) {
            $this->menuItem = MenuItemStorage::get($this->menuItemId);
        }
        return $this->menuItem;
    }

    public function getInputFields()
    {
        return ['id', 'order_id', 'menu_item_id', 'status'];
    }

    public function getOutputFields()
    {
        return [
            'id' => function (OrderItem $orderItem) {
                return $orderItem->getId();
            },
            'name' => function (OrderItem $orderItem) {
                return $orderItem->getMenuItem()->getName();
            },
            'source' => function (OrderItem $orderItem) {
                $sourceId = $orderItem->getMenuItem()->getSourceId();
                return SourceStorage::get($sourceId)->getName();
            },
            'status' => function (OrderItem $orderItem) {
                if ($orderItem->getStatus() == OrderItem::STATUS_NEW) {
                    return 'New';
                } elseif ($orderItem->getStatus() == OrderItem::STATUS_PROGRESS) {
                    return 'Progress';
                } elseif ($orderItem->getStatus() == OrderItem::STATUS_READY) {
                    return 'Ready';
                } elseif ($orderItem->getStatus() == OrderItem::STATUS_SERVED) {
                    return 'Served';
                }
                return 'Payed';
            }
        ];
    }
}