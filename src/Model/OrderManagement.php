<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 20.12.2018 г.
 * Time: 16:30
 */

namespace ClubSoftware\Model;

use ClubSoftware\Storage\OrderItemStorage;

abstract class OrderManagement
{
    private static $waitress;
    private static $bar;
    private static $kitchen;
    private static $acceptedItems = null;

    /**
     * @return mixed
     */
    public static function getWaitress()
    {
        if (!self::$waitress) {
            self::$waitress = new Waitress(1, 'Kalina');
        }
        return self::$waitress;
    }

    /**
     * @return mixed
     */
    public static function getBar()
    {
        if (!self::$bar) {
            self::$bar = new Bar();
        }
        return self::$bar;
    }

    /**
     * @return mixed
     */
    public static function getKitchen()
    {
        if (!self::$kitchen) {
            self::$kitchen = new Kitchen();
        }
        return self::$kitchen;
    }

    public static function manageItem($orderItem, $source)
    {
        $prompt = "ClubSoftware Order Management Interface 
                  Choose Class for Object: 
                  1. Accept Item 
                  2. Deny Item
                  ";

            echo $prompt;
            $choice = readline();
        if ($choice != 1 && $choice != 2) {
            throw new \InvalidArgumentException('Choose between 1 or 2!');
        }
        switch ($choice) {
            case 1:
                self::accept($orderItem, $source);
                break;
            case 2:
                self::deny($orderItem, $source);
                break;
        }
        return true;
    }

    public static function sendToSource(array $orderItems)
    {
        $phpEol = PHP_EOL;
        $barObject = self::getBar();
        $kitchenObject = self::getKitchen();
        self::$waitress = self::getWaitress();

        foreach ($orderItems as $key => $value) {
            $destination = $value->getMenuItem()->getSourceId();
            $name = $value->getMenuItem()->getName();

            if ($destination == MenuItem::SOURCE_BAR) {
                $destination = $barObject;
            } elseif ($destination == MenuItem::SOURCE_KITCHEN) {
                $destination = $kitchenObject;
            }

            $source = $destination->getClassName();
            echo "Does $source accept $name? $phpEol";
            $orderItems[$key] = OrderManagement::manageItem($orderItems[$key], $source);
        }
        if (!static::$acceptedItems) {
            echo "No Items Accepted! Order is Empty!" . PHP_EOL;
            return false;
        }
        return true;
    }

    public static function accept($orderItem, $source)
    {
        $orderItem->setStatus(OrderItem::STATUS_PROGRESS);
        echo "$source accepted " . $orderItem->getMenuItem()->getName() . PHP_EOL;
        OrderItemStorage::add($orderItem);
        static::$acceptedItems = 1;
    }

    public static function deny($orderItem, $source)
    {
        $orderItem->setStatus(OrderItem::STATUS_NEW);
        echo "$source denied " . $orderItem->getMenuItem()->getName() . PHP_EOL;
        echo $orderItem->getMenuItem()->getName() . " is out of stock!" . PHP_EOL;
        //Waitress::$deniedItems[] = $orderItem;
    }
}