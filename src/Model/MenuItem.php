<?php
namespace ClubSoftware\Model;

use ClubSoftware\Storage\CategoryStorage;
use ClubSoftware\Storage\SourceStorage;

class MenuItem implements InputFields, OutputFields
{
    private $id;
    private $name;
    private $description;
    private $price;
    private $source;
    private $imgPath;
    private $categoryId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getSourceId()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSourceId($source)
    {
        $this->source = $source;
    }

    /**
     * @return mixed
     */
    public function getImgPath()
    {
        return $this->imgPath;
    }

    /**
     * @param mixed $imgPath
     */
    public function setImgPath($imgPath)
    {
        $this->imgPath = $imgPath;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    public function getInputFields()
    {
        return ['name', 'description', 'price', 'source', 'categoryId'];
    }

    public function getOutputFields()
    {
        return [
            'id' => function (MenuItem $menuItem) {
                return $menuItem->getId();
            },
            'name' => function (MenuItem $menuItem) {
                return $menuItem->getName();
            },
            'description' => function (MenuItem $menuItem) {
                return $menuItem->getDescription();
            },
            'price' => function (MenuItem $menuItem) {
                return $menuItem->getPrice();
            },
            'source' => function (MenuItem $menuItem) {
                $source = SourceStorage::get($menuItem->getSourceId());
                return $source->getName();
            },
            'category' => function (MenuItem $menuItem) {
                $category = CategoryStorage::get($menuItem->getCategoryId());
                return $category->getName();
            },
        ];
    }
}
