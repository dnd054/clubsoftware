<?php
namespace ClubSoftware\Model;

class Table implements OutputFields
{
    private $id;
    private $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getInputFields()
    {
        return ['id', 'name'];
    }

    public function getOutputFields()
    {
        return [
            'id'  => function (Table $table) {
                return $table->getId();
            },
            'name'  => function (Table $table) {
                return $table->getName();
            }
        ];
    }
}