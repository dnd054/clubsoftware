<?php
namespace ClubSoftware\Model;

class Kitchen
{
    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}