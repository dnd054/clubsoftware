<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 12.4.2019 г.
 * Time: 17:11
 */

namespace ClubSoftware\Model;

use ClubSoftware\Storage\UserStorage;

class Source
{
    private $id;
    private $userId;
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getOutputFields()
    {
        return [
            'id' => function (Source $source) {
                return $source->getId();
            },
            'name' => function (Source $source) {
                return $source->getName();
            },
            'username' => function (Source $source) {
                $user = UserStorage::get($source->getUserId());
                return $user->getUserName();
            }
        ];
    }
}