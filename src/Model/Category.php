<?php
/**
 * Created by PhpStorm.
 * User: DND
 * Date: 27.3.2019 г.
 * Time: 15:33
 */

namespace ClubSoftware\Model;

use ClubSoftware\Helper\CategoriesHelper;

class Category implements InputFields, OutputFields
{
    private $id;
    private $parentId;
    private $name;
    private $path;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param mixed $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getInputFields()
    {
        return ['id', 'parent_id', 'name', 'path'];
    }

    public function getOutputFields()
    {
        return [
            'id' => function (Category $category) {
                return $category->getId();
            },
            'parent_id' => function (Category $category) {
                return $category->getParentId();
            },
            'name' => function (Category $category) {
                return $category->getName();
            },
            'path' => function (Category $category) {
                $categories = CategoriesHelper::getCategories($category->getPath());
                $names = array_map(function ($value) {
                    return $value->getName();
                }, $categories);
                return implode(CategoriesHelper::PATH_SEPARATOR, $names);
            }
        ];
    }
}