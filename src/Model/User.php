<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 24.02.19
 * Time: 16:03
 */

namespace ClubSoftware\Model;

use ClubSoftware\Helper\FormatDateTime;

class User implements OutputFields
{
    const TYPE_CLIENT = 1;
    const TYPE_ADMIN = 2;
    const TYPE_SOURCE = 3;
    const TYPE_PERSONEL = 4;
    const ACTIVE = 1;
    const INACTIVE = 0;

    private $id;
    private $username;
    private $password;
    private $role;
    private $lastActive;
    private $status;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getLastActive()
    {
        return $this->lastActive;
    }

    /**
     * @param mixed $lastActive
     */
    public function setLastActive($lastActive)
    {
        $this->lastActive = $lastActive;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getOutputFields()
    {
        return [
            'id' => function (User $user) {
                return $user->getId();
            },
            'username' => function (User $user) {
                return $user->getUsername();
            },
            'role' => function (User $user) {
                switch ($user->getRole()) {
                    case User::TYPE_CLIENT:
                        return 'Client';
                    case User::TYPE_ADMIN:
                        return 'Admin';
                    case User::TYPE_SOURCE:
                        return 'Source';
                }
                return 'Personel';
            },
            'last_active' => function (User $user) {
                return FormatDateTime::toTable($user->getLastActive());
            },
            'status' => function (User $user) {
                if ($user->getStatus() == User::ACTIVE) {
                    return 'Active';
                }
                return 'Inactive';
            }
        ];
    }
}
