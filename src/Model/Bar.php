<?php
namespace ClubSoftware\Model;

class Bar
{
    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}